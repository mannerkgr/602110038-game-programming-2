﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyScriptableObject : ScriptableObject
{
    public string _objectName = "My Object";
    public float _speedMagnitude = 1.0f;
    public float _firerate = 1.0f;
    public float _HP = 1.0f;
    // Start is called before the first frame update
   
}
