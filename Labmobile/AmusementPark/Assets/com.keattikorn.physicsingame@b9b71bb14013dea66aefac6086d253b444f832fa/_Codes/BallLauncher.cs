﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallLauncher : MonoBehaviour
{
    [SerializeField]
    float _chargeSpeed = 100;
        
    [SerializeField]
    Slider _slider;


    [SerializeField]
    float _currentThetaAngle = 90;

    [SerializeField]
    float _currentPhiAngle = 90;

    
    float _length = 1.5f;
    const float ANGLE_STEP = 3;

    Vector3 _initialPosition;
    public float _ballMass=1;
    public float _impulseForce;
    // Use this for initialization
    void Start()
    {
        _initialPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float sine = Mathf.Sin(_currentThetaAngle * Mathf.Deg2Rad);
        float cosine = Mathf.Cos(_currentThetaAngle * Mathf.Deg2Rad);
        float sinePhi = Mathf.Sin(_currentPhiAngle * Mathf.Deg2Rad);
        float cosinePhi = Mathf.Cos(_currentPhiAngle * Mathf.Deg2Rad);
        Vector3 unitCircleDirection = new Vector3(_length * cosine * sinePhi, _length* cosinePhi, _length * sine* sinePhi);
        this.transform.position = _initialPosition + unitCircleDirection;

        if (Input.GetKey(KeyCode.LeftArrow))
            _currentThetaAngle += ANGLE_STEP;
        if (Input.GetKey(KeyCode.RightArrow))
            _currentThetaAngle -= ANGLE_STEP;
        if (Input.GetKey(KeyCode.UpArrow))
            _currentPhiAngle -= ANGLE_STEP;
        if (Input.GetKey(KeyCode.DownArrow))
            _currentPhiAngle += ANGLE_STEP;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _slider.value = 0;
        } else if (Input.GetKey(KeyCode.Space)) {
            _slider.value += Time.deltaTime * _chargeSpeed;
        } else if (Input.GetKeyUp(KeyCode.Space)) {
            _impulseForce = _slider.value;

            GameObject sphereBall = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphereBall.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            sphereBall.transform.position = (_initialPosition + unitCircleDirection * 1.5f);
            Rigidbody rb = sphereBall.AddComponent<Rigidbody>();
            rb.mass = _ballMass;
            rb.AddForce(unitCircleDirection.normalized * _impulseForce, ForceMode.Impulse);
            rb.collisionDetectionMode = CollisionDetectionMode.Discrete;

            Light li = sphereBall.AddComponent<Light>();
            li.type = LightType.Point;
            li.intensity = 1.5f;

            Destroy(sphereBall, 5);            
        }
        Debug.DrawLine(_initialPosition, this.transform.position,Color.white);
    }


}

