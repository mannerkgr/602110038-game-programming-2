﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour
{
    public float speed = 1;
    public float limit_CoordinateX = -4.4f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManagement.instance.isStartGame)
        {
            this.transform.position -= new Vector3(speed * Time.deltaTime, 0, 0);
            if (this.transform.position.x < limit_CoordinateX)
            {
                this.transform.position = new Vector3(0, 0, 0);
            }
        }
    }
}
