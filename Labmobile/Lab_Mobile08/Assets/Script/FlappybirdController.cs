﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappybirdController : MonoBehaviour
{
    public float JumpForce = 150;
    public bool isDead = false;
    Rigidbody2D _body;
    // Start is called before the first frame update
    private void Awake()
    {
        _body = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) ||
 Input.touchCount > 0 &&
 Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (!isDead)
            {
                _body.velocity = new Vector2(0, 0);
                _body.AddForce(new Vector2(0, JumpForce));
                AudioManagement.instance.PlayWing();
            }
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        print("Trigger : " + other.name);
        if (other.name == "scoreUp")
        {
            GameManagement.instance.Score++;
        }
        else
        {
            isDead = true;
            AudioManagement.instance.PlayLost();
            SettingSimulated();
        }
    }
    public void SettingSimulated()
    {
        _body.simulated = GameManagement.instance.isStartGame;
    }
}
