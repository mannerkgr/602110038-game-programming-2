﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagement : MonoBehaviour
{
    public bool isStartGame = false;
    [SerializeField]
    private int score = 0;
    public int Score
    {
        get { return score; }
        set
        {
            //SetScore (value);
            score = value;
            UIManagement.instance.SetScore(score, isStartGame);
            AudioManagement.instance.PlayPlusPoint();
        }
    }
    public StageController m_background;
    public StageController m_floor;
    public PipeController m_pipesControl;
    public FlappybirdController m_flappbird;
    public static GameManagement instance = null;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    void Start()
    {
        isStartGame = true;
        SetupControl();
    }
    // Update is called once per frame
    void Update()
    {
        if (m_flappbird.isDead)
        {
            isStartGame = false;
            SetupControl();
        }
    }
    void SetupControl()
    {
        m_flappbird.SettingSimulated();
        UIManagement.instance.SetupControl(isStartGame);
        UIManagement.instance.SetScore(score, isStartGame);
    }
    public void ResetLevel()
    {
        SceneManager.LoadScene("GamePlay");
    }
}