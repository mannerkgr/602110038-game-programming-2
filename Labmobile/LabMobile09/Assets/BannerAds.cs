﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class BannerAds : MonoBehaviour
{
    public string bannerPlacement = "banner";
    public bool testMode = false;
    // Start is called before the first frame update
#if UNITY_IOS
 public const string gameID = "3352049";
#elif UNITY_ANDROID
 public const string gameID = "3352048";
#elif UNITY_EDITOR
    public const string gameID = "1111111";


#endif

    void Start()
    {
        Advertisement.Initialize(gameID, testMode);
        StartCoroutine(ShowBannerWhenReady());
    }
    IEnumerator ShowBannerWhenReady()
    {
        while (!Advertisement.IsReady("banner"))
        {
            yield return new WaitForSeconds(0.5f);
        }
        Advertisement.Banner.Show(bannerPlacement);
    }
}