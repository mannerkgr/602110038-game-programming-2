﻿using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.UI;
public class LoginPlayFabUser : MonoBehaviour
{
    public Text textUser;
    public Text textPassword;
    public void UILoginPlayFabWithUserAndPassword()
    {
        LoginPlayFabWithUserAndPassword(textUser.text, textPassword.text);
    }
    void LoginPlayFabWithUserAndPassword(string username, string password)
    {
        var request = new LoginWithPlayFabRequest()
        {
            Username = username,
            Password = password
            /*
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams()
            {
            // And make sure PlayerProfile is included
            GetPlayerProfile = true,
            // Define rules for PlayerProfile request
            ProfileConstraints = new PlayerProfileViewConstraints()
            {
            // And make sure that both AvatarUrl and LastLogin are included.
           ShowAvatarUrl = true,
           ShowLastLogin = true,
            }
            }
            */
        };
        PlayFabClientAPI.LoginWithPlayFab(request,
        OnLoginSuccess,
        OnRegisterFailure);
    }
    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("The player's profile Created date is: " + result.PlayFabId);
    }
    private void OnRegisterFailure(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your first API call. :(");
        Debug.LogError("Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
    }
}
