﻿using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;
public class FacebookAuth : MonoBehaviour
{
    // holds the latest message to be displayed on the screen
    private string _message;
    public GameObject btnLogin;
    public RawImage ImgProfile;
    // Use this for initialization
    void Start()
    {
        SetMessage("Initializing Facebook..."); // logs the given message and displays it on the screen using OnGUI method
 // This call is required before any other calls to the Facebook API. We pass in the callback to be invoked once initialization is finished
 FB.Init(OnFacebookInitialized);
    }
    private void OnFacebookInitialized()
    {
        SetMessage("Logging into Facebook...");
        // Once Facebook SDK is initialized, if we are logged in, we log out demonstrate the entire authentication cycle.
    if (FB.IsLoggedIn)
            FB.LogOut();
    }
    public void UILoginWithFacebook()
    {
        // We invoke basic login procedure and pass in the callback to process the result
         var perms = new List<string>() { "public_profile", "email", "user_friends"
        };
        FB.LogInWithReadPermissions(perms, OnFacebookLoggedIn);
    }
    private void OnFacebookLoggedIn(ILoginResult result)
    {
        // If result has no errors, it means we have authenticated in Facebook
        //successfully
    if (result == null || string.IsNullOrEmpty(result.Error))
        {
            if (FB.IsLoggedIn)
            {
                SetMessage("Facebook Auth Complete! Access Token: " +
               AccessToken.CurrentAccessToken.TokenString + "\nLogging into PlayFab...");
                foreach (string perm in AccessToken.CurrentAccessToken.Permissions)
                {
                }
                /*
                * We proceed with making a call to PlayFab API. We pass in current
               Facebook AccessToken and let it create
                * and account using CreateAccount flag set to true. We also pass
               the callback for Success and Failure results
                */
                LoginWithFacebookRequest fbRequest = new LoginWithFacebookRequest
                {
                    CreateAccount = true,
                    AccessToken = AccessToken.CurrentAccessToken.TokenString
                };

                PlayFabClientAPI.LoginWithFacebook(fbRequest,
                OnPlayfabFacebookAuthComplete, OnPlayfabFacebookAuthFailed);
            }
            else
            {
                Debug.Log("User cancelled login");
            }
        }
        else
        {
            // If Facebook authentication failed, we stop the cycle with the message
            SetMessage("Facebook Auth Failed: " + result.Error + "\n" +
           result.RawResult, true);
        }
    }
    protected virtual void
   OnPlayfabFacebookAuthComplete(PlayFab.ClientModels.LoginResult result)
    {
        SetMessage("PlayFab Facebook Auth Complete. Session ticket: " +
       result.SessionTicket);
        btnLogin.SetActive(false);
        ImgProfile.gameObject.SetActive(true);
        FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET,
       ProfilePhotoCallback);
    }
    private void ProfilePhotoCallback(IGraphResult result)
    {
        this.ImgProfile.texture = result.Texture;
    }
    private void OnPlayfabFacebookAuthFailed(PlayFabError error)
    {
        SetMessage("PlayFab Facebook Auth Failed: " + error.GenerateErrorReport(),
       true);
    }
    public void SetMessage(string message, bool error = false)
    {
        _message = message;
        if (error)
            Debug.LogError(_message);
        else
            Debug.Log(_message);
    }
}