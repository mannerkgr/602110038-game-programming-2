﻿using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.UI;
public class RegisterPlayFabUser : MonoBehaviour
{
    public Text textUser;
    public Text textPassword;
    public Text textEmail;
    public void UIRegisterPlayFabUserWithUserAndPassword()
    {
        RegisterPlayFabUserWithUserAndPassword(textUser.text, textPassword.text,
       textEmail.text);
    }
    void RegisterPlayFabUserWithUserAndPassword(string username, string password,
   string email)
    {
        var request = new RegisterPlayFabUserRequest()
        {
            Username = username,
            Password = password,
            Email = email,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams()
            {
                // And make sure PlayerProfile is included
                GetPlayerProfile = true,
                // Define rules for PlayerProfile request
                ProfileConstraints = new PlayerProfileViewConstraints()
                {
                    // And make sure that both AvatarUrl and LastLogin are included.
                    ShowAvatarUrl = true,
                    ShowLastLogin = true,
                }
            }
        };
        PlayFabClientAPI.RegisterPlayFabUser(request,
        OnRegisterSuccess,
        OnRegisterFailure);
    }
    private void OnRegisterSuccess(RegisterPlayFabUserResult result)
    {
        Debug.Log("The player's profile Created date is: " + result.Username);
    }
    private void OnRegisterFailure(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your first API call. :(");
        Debug.LogError("Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
    }
}
