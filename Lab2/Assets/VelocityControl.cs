﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityControl : MonoBehaviour
{
    [SerializeField]
    private Vector3 _velocity;
    public Vector3 Velocity{
     get{   
         return _velocity;
     }
     set{
         this._velocity = value;
     }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += _velocity * Time.deltaTime;
    }
}
