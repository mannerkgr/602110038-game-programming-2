﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootFromQuadNormalVector : MonoBehaviour
{
    Vector3 _movementVector = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update(){
    this.transform.position += _movementVector;

if (Input.GetMouseButtonDown(0)) {

 GameObject quad = GameObject.Find("Quad");
 Mesh mesh = quad.GetComponent <MeshFilter >().mesh;
 List<Vector3> vertices = new List<Vector3 >() ;
 mesh.GetVertices(vertices);

 Vector3 v1 = quad.transform.TransformPoint(vertices[0]) - quad.
transform.TransformPoint(vertices[1]);
 Vector3 v2 = quad.transform.TransformPoint(vertices[0]) - quad.
transform.TransformPoint(vertices[2]);
 Vector3 vNormal = Vector3.Cross(v1, v2).normalized;
 _movementVector = vNormal*0.1f;
 this.transform.position = quad.transform.position;
 }
 }

 }