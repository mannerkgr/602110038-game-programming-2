﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeControl : MonoBehaviour
{
    Vector3 _cubeMovementStep = new Vector3(0.03f,0.05f,0);
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += _cubeMovementStep;
        if (this.transform.position.x >= 2.0f || this.transform.position.x <= -2.0f) 
        {
        _cubeMovementStep.x *= -1;
        }
        if (this.transform.position.y >= 2.0f || this.transform.position.y <= -2.0f){
        _cubeMovementStep.y *= -1;
    }
    if (this.transform.position.z >= 2.0f || this.transform.position.z <= -2.0f){
        _cubeMovementStep.z *= -1;
    }
    }
}
