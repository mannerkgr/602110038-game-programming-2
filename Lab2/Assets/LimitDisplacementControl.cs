﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VelocityControl))]
public class LimitDisplacementControl : MonoBehaviour
{
    [SerializeField]
    private float _maximumDisplacement;
    private float _currentDisplacement;
    // Start is called before the first frame update
    void Start()
    {
        _currentDisplacement = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = this.GetComponent<Transform>().position;
        VelocityControl vc = GetComponent <VelocityControl >();
        Vector3 v = vc.Velocity;
        _currentDisplacement += (v*Time.deltaTime).magnitude;
        if(_currentDisplacement >= _maximumDisplacement){
            _currentDisplacement = 0;
            v *= -1;
            vc.Velocity = v;
        }
    }
}
