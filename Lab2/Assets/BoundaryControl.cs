﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VelocityControl))]
public class BoundaryControl : MonoBehaviour
{
    [SerializeField]
      
   private float _boundary;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = this.GetComponent <Transform >().position;
        VelocityControl vc = GetComponent <VelocityControl >();
        Vector3 tmpVelocity = vc.Velocity;

        if (pos.x >= _boundary || pos.x <= -_boundary)
        {
            tmpVelocity.x *= -1;
            vc.Velocity = tmpVelocity;
        }
        if (pos.y >= _boundary || pos.y <= -_boundary)
        {
            tmpVelocity.y *= -1;
            vc.Velocity = tmpVelocity;
        }
        if (pos.z >= _boundary || pos.z <= -_boundary)
        {
            tmpVelocity.z *= -1;
            vc.Velocity = tmpVelocity;
        }
    }
}
