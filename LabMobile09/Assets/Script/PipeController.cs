﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeController : MonoBehaviour
{
    public float speed = 1;
    public float limit_CoordinateX = -2.4f;

    public float limit_down = -0.5f;
    public float limit_top = 1.75f;
    public float resetTo = 0.8f;

    [SerializeField]
    private Transform[] m_pipes;
    // Start is called before the first frame update
    void Start()
    {
        m_pipes = new Transform[this.transform.childCount];
        for (int i = 0; i < m_pipes.Length; i++)
        {
            m_pipes[i] = this.transform.GetChild(i);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManagement.instance.isStartGame)
        {
            for (int i = 0; i < m_pipes.Length; i++)
            {
                m_pipes[i].position -=
                new Vector3(speed * Time.deltaTime, 0, 0);
                if (m_pipes[i].position.x <
                limit_CoordinateX)
                {
                    m_pipes[i].position =
                     new Vector3(resetTo,
                     Random.Range(limit_down, limit_top), 0);
                }
            }
             }
        }
    }

