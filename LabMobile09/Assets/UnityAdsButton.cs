﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Monetization;
[RequireComponent(typeof(Button))]
public class UnityAdsButton : MonoBehaviour
{
    public string placementId = "rewardedVideo";
    private Button adButton;
   // #if UNITY_IOS
     private string gameId = "3352049";
    //#elif UNITY_ANDROID
    //private string gameId = "3347212";
    //#endif
    void Start()
    {
        adButton = GetComponent<Button>();
        if (adButton)
        {
            adButton.onClick.AddListener(ShowAd);
        }
        if (Monetization.isSupported)
        {
            Monetization.Initialize(gameId, true);
        }
    }
    void Update()
    {
        if (adButton)
        {
            adButton.interactable = Monetization.IsReady(placementId);
        }
    }
    void ShowAd()
    {
        ShowAdCallbacks options = new ShowAdCallbacks();
        options.finishCallback = HandleShowResult;
        ShowAdPlacementContent ad = Monetization.GetPlacementContent(placementId) as
       ShowAdPlacementContent;
        ad.Show(options);
    }
    void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                Debug.Log("Player Gains +5 gems");
                // YOUR CODE TO REWARD THE GAMER
                break;
            case ShowResult.Skipped:
                Debug.Log("Player did not fully watch the ad.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}