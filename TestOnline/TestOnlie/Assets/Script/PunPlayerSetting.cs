﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunPlayerSetting 
{
    public const float MIN_SPAWN_TIME = 0.5f;
    public const float MAX_SPAWN_TIME = 1.0f;

    public const float PLAYER_RESPAWN_TIME = 2.0f;

    public const string PLAYER_READY = "IsPlayerReady";
    public const string PLAYER_LOADED_LEVEL = "PlayerLoadLevel";
    public const string PLAYER_COLOR = "PlaterColor";

    public const string START_GAMETIME = "StartGame";
    public const string GAMEOVE = "IsGameOver"; 
}
