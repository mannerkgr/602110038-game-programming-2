﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using System;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class LoadingLevel : MonoBehaviour
{
    public bool SettetingCountTime;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start Game Is Working");
        PunNetworkManager.singleton.Counttime = SettetingCountTime;
        Debug.Log("Start Game Is Now" + PunNetworkManager.singleton.Counttime);
        
     //  GameObject.Find("PunNetworkManager").GetComponent<PunNetworkManager>().SpawnPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoadNextScene(string sceneName)
    {
        if (sceneName == "Lobby" )
        {
            PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.LocalPlayer);
        }
        //Counttime = true;
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(sceneName);
            //StartGame();
            // PhotonNetwork.JoinRandomRoom();
        }
        else GameObject.Find("NextLevelButton").SetActive(false);
    }
}
