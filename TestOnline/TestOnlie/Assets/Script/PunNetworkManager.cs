﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using System;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;
using ExitGames.Client.Photon;
public class PunNetworkManager : ConnectAndJoinRandom
{
    bool IsGameStart = false;
    public static PunNetworkManager singleton;
    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerL04;
    public GameObject GamePlayerL38;
    public bool Counttime;
    public float TimetoSpawn;
    // if (PhotonNetwork.PlayerList.Length <= 1)
    private void StartGame()
    {
        Camera main = Camera.main;
        if (main.name == "Camera")
            main.gameObject.SetActive(false);
        SpawnPlayer();
        Counttime = false;
        TimetoSpawn = 0;

    }
    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        singleton = this;
        // StartGame();
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Hashtable playerProps = PhotonNetwork.LocalPlayer.CustomProperties;
        if (!playerProps.ContainsKey(PunPlayerSetting.PLAYER_LOADED_LEVEL))
        {
            PlayerProperties(PhotonNetwork.PlayerList.Length);
        }
        // StartGame();
        //Camera.main.gameObject.SetActive(false);
        /* if (PunNetworkControlForAll.LocalPlayerInstance == null)
         {
             Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
             PunNetworkManager.singleton.SpawnPlayer();
         }
         else
         {
             Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }*/
    }
    private void OnCountdownTimerIsExpired()
    {
        if (AutoConnect == false)
            StartGame();
    }
    public void SpawnPlayer()
    {
        // Debug.Log("Spawn Player is Workiing");
        
            if (NetworkPlayer.LocalPlayerInstance == null)
        {
            IsGameStart = true;
            if (SettingPlayerProperties () == 1)
            {
                PhotonNetwork.Instantiate(GamePlayerL04.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
            }
           else if (SettingPlayerProperties() == 2)
            {
                PhotonNetwork.Instantiate(GamePlayerL38.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
            }

        }


    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //   PhotonNetwork.DestroyPlayerObjects(PhotonNetwork);
            PhotonNetwork.LeaveRoom();
            PhotonNetwork.LoadLevel(0);
        }
        if (PunNetworkManager.singleton.Counttime == true)
        {
            TimetoSpawn += Time.deltaTime;

            if (TimetoSpawn >= 1)
            {
                StartGame();
            }
        }



    }
    public void QuitApplication()
    {
        Application.Quit();
    }

    public void LoadNextScene(string sceneName)
    {
        //Counttime = true;
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(sceneName);


            //StartGame();
            // PhotonNetwork.JoinRandomRoom();
        }
        else GameObject.Find("NextLevelButton").SetActive(false);
    }
    private void PlayerProperties(int Character)
    {

        Hashtable props = new Hashtable
        {
            {PunPlayerSetting.PLAYER_LOADED_LEVEL,Character}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    }
    public int SettingPlayerProperties()
    {
        Hashtable playerProps = PhotonNetwork.LocalPlayer.CustomProperties;
        if (playerProps.ContainsKey(PunPlayerSetting.PLAYER_LOADED_LEVEL))
        {
            object CharacterType;
            if (playerProps.TryGetValue(PunPlayerSetting.PLAYER_LOADED_LEVEL, out CharacterType))
            {
               return (int)CharacterType;
                
            }

        }
        return 0;
    }

}
