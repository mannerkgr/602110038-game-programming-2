﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwitch : MonoBehaviour
{
    [SerializeField]
    GameObject Door;
    bool isOpen = false;
    // Start is called before the first frame update
   public void OnTriggerStay(Collider other)
    {
        if (!isOpen)
        {
            isOpen = true;
            Door.transform.position += new Vector3(0, 6, 0);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (isOpen)
        {
            isOpen = false;
            Door.transform.position += new Vector3(0, -6, 0);
        }
    }
    

}
