﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using System;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class LobbyManager : ConnectAndJoinRandom
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoadLevel1(string sceneName)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(sceneName);
        }
        else GameObject.Find("Level_1").SetActive(false);
    }
    public void LoadLevel2(string sceneName)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(sceneName);
        }
        else GameObject.Find("Level_2").SetActive(false);
    }
    public void LoadLevel3(string sceneName)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(sceneName);
        }
        else GameObject.Find("Level_3").SetActive(false);
    }
    public void LoadLevel4(string sceneName)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(sceneName);
        }
        else GameObject.Find("Level_4").SetActive(false);
    }
    public void LoadLevel5(string sceneName)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(sceneName);
        }
        else GameObject.Find("Level_5").SetActive(false);
    }
}
