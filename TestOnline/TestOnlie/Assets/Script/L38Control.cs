﻿using Photon.Pun.Demo.PunBasics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class L38Control : MonoBehaviourPunCallbacks, IPunObservable
{
    private Rigidbody rg;
    public float moveSpeed;
    [SerializeField]
    public float jumpSpeed;
    // Start is called before the first frame update

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // throw new System.NotImplementedException();
    }
    void Start()
    {
        rg = this.GetComponent<Rigidbody>();
        CameraWork cameraWork = gameObject.GetComponent<CameraWork>();

        if (cameraWork != null)
        {
            if (photonView.IsMine)
            {
                cameraWork.OnStartFollowing();
            }
        }
        else
        {
            Debug.LogError("<Color=Red><b>Missing</b></Color> CameraWork Component on player Prefab.", this);
        }
    }

    // Update is called once per frame
       void Update()
        {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 atas = new Vector3(0, 80, 0);
            rg.AddForce(atas * moveSpeed);
        }
           
         UpdateInput();
        }
    void UpdateInput()
    {
       // float hori = Input.GetAxis("Horizontal");
        //float verti = -1 * Input.GetAxis("Vertical");

       // Vector3 unitPositiveHori = new Vector3(0, 0, 1);
       // Vector3 unitPositiveVerti = new Vector3(1, 0, 0);

       // rg.AddForce(unitPositiveHori * hori * moveSpeed, ForceMode.Force);
      //  rg.AddForce(unitPositiveVerti * verti * moveSpeed, ForceMode.Force);
        
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontal, 0, vertical);

        gameObject.transform.Translate(direction.normalized * Time.deltaTime * moveSpeed);

        // if(Input.GetKeyDown(KeyCode.Space)){
        //     _rigidbody.AddForce ()
        // }
    }

}
