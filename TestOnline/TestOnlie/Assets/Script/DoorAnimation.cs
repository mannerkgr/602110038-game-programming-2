﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

//[RequireComponent(typeof(PhotonTransformView))]
//[RequireComponent(typeof(PhotonRigidbodyView))]
public class DoorAnimation : MonoBehaviour//Pun, IPunInstantiateMagicCallback
{
    public Animator animator ;
    //public static bool ispressed = false;
    public static bool IsPressed ;
    // Start is called before the first frame update

   


    void Start()
    {
        ////  if (!photonView.IsMine)
        //  return;
        IsPressed = false;
        animator = GetComponent<Animator>();
    } 
    

    
    // Update is called once per frame
    void Update()
    {
        /*  if(Input.GetKeyDown(KeyCode.N) && !IsPressed)
         {
             animator.SetBool("IsPressed", true);
             IsPressed = true;

         }
         else if (Input.GetKeyDown(KeyCode.M) && IsPressed)
         {
             animator.SetBool("IsPressed", false);
             IsPressed = false;

         }
         */
        if (IsPressed) 
        {
            //    Debug.Log("The Animation is working");
            animator.SetBool("IsPressed", true);
           
        }
     else if (!IsPressed )
        {
            //  Debug.Log("The Animation is not working");
            animator.SetBool("IsPressed", false);
       }
    }

    // public void OnPhotonInstantiate(PhotonMessageInfo info)
    // {
    // info.Sender.TagObject = this.gameObject;
    //}
}
