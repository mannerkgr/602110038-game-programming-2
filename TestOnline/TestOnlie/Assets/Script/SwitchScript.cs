﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchScript : MonoBehaviour
{
    
    
    // Start is called before the first frame update
    void Start()
    {
        DoorAnimation.IsPressed = false;
        // lockeddoor.SetActive(true);
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "L04" )
        {
            DoorAnimation.IsPressed = true;
            //    Debug.Log("The Switch has been pressed by L04");
        }
        if (other.gameObject.tag == "Box")
        {
            DoorAnimation.IsPressed = true;
            //    Debug.Log("The Switch has been pressed by BOXES");
        }
        if (other.gameObject.tag == "L38")
        {
            DoorAnimation.IsPressed = true;
            //    Debug.Log("The Switch has been pressed by L38");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        DoorAnimation.IsPressed = false;
        //     Debug.Log("The Switch has not been pressed");
    }
    // private void OnCollisionStay(Collision collision)
    // {


    // DoorAnimation.IsPressed = true;
    // Debug.Log("The Switch has been pressed");
    //  lockeddoor.SetActive(false);
    //// lockeddoor.transform.position += new Vector3(0, 1.5f, 0);

    //}
    //private void OnCollisionExit(Collision collision)
    // {

    //  DoorAnimation.IsPressed = false;
    // Debug.Log("The Switch has not been pressed");
    //lockeddoor.SetActive(true);

    //}

    // Update is called once per frame
    void Update()
    {
       
    }
}
