﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine;
using Photon.Pun;



[RequireComponent(typeof(PhotonTransformView))]

public class PunUserNetControl : MonoBehaviourPunCallbacks
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
  public static GameObject LocalPlayerInstance;
    private void Awake()
    {
        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
    if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
        }
        else
        {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponent<L38Control>().enabled = false;
        }
    }
}