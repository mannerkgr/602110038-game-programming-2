﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.Demo.PunBasics;


[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControlL38 : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());

        //2 Player
        Debug.Log("Phonton is Working On 88");
        if (info.photonView.ViewID >= 1000 && info.photonView.ViewID < 2000)
        {
            Debug.Log("Phonton is Working");
        }
        else if (info.photonView.ViewID >= 2000)
        {
            Debug.Log("Photon 2 is Working");
        }
        // 2 Player

        //throw new System.NotImplementedException();
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
            GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
        {
            GetComponentInChildren<CameraWork>().enabled = false;
            // GetComponentInChildren<AudioListener>().enabled = false;
           // GetComponent<L38Control>().enabled = false;
            GetComponent<L04Control>().enabled = false;
        }


    }

    private void Awake()
    {

        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
        }
        else
        {
            GetComponentInChildren<CameraWork>().enabled = false;
           // GetComponent<L38Control>().enabled = false;
            GetComponent<L04Control>().enabled = false;
        }
    }
}