﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.Demo.PunBasics;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;

[RequireComponent(typeof(PhotonTransformView))]
public class NetworkPlayer : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;
    public GameObject myCamera;
    public Text _textNickname;

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());

        //2 Player
        Debug.Log("Phonton is Working On 88");
        if (info.photonView.ViewID >= 1000 && info.photonView.ViewID < 2000)
        {
            Debug.Log("Phonton is Working");
            GetComponent<L04Control>().enabled = false;
        }
        else if (info.photonView.ViewID >= 2000)
        {
            Debug.Log("Photon 2 is Working");

            GetComponent<L38Control>().enabled = false;
        }
        // 2 Player

        //throw new System.NotImplementedException();
        if (photonView.IsMine)
        {
            info.Sender.TagObject = this.gameObject;
            LocalPlayerInstance = gameObject;
            GetComponent<MeshRenderer>().material.color = Color.green;
            GetComponentInChildren<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;


        }
        else
        {
            //GetComponentInChildren<CameraWork>().enabled = false;
            // GetComponentInChildren<AudioListener>().enabled = false;
            // myCamera.SetActive(false);
            GetComponent<PlayerMovement>().enabled = false;
            GetComponentInChildren<Camera>().enabled = false;
            // GetComponent<L04Control>().enabled = false;
        }
        if (_textNickname != null)
            _textNickname.text = info.Sender.NickName;


    }
    private void Awake()
    {
        //DontDestroyOnLoad(this.transform.parent.gameObject);
        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
        }
        else
        {

            // GetComponentInChildren<CameraWork>().enabled = false;
            //GetComponent<L38Control>().enabled = false;
            //GetComponent<L04Control>().enabled = false;
            // myCamera.SetActive(false);
            GetComponent<PlayerMovement>().enabled = false;
            GetComponentInChildren<Camera>().enabled = false;

        }
    }
   
}