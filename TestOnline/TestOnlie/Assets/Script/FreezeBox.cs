﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonTransformView))]
[RequireComponent(typeof(PhotonRigidbodyView))]
public class FreezeBox : MonoBehaviourPun, IPunInstantiateMagicCallback

{ 
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        //throw new System.NotImplementedException();
        info.Sender.TagObject = this.gameObject;
    }
    Rigidbody m_Rigidbody;
    Vector3 m_YAxis;
   
    // Start is called before the first frame update
    void Start()
    {
      

        m_Rigidbody = GetComponent<Rigidbody>();
        m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;

    }
    private void OnCollisionExit(Collision collision)
    {
        if (!photonView.IsMine)
            return;

        //   Debug.Log("Stilling");
        // Rigidbody.isKinematic = false;
        m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
    }

    private void OnCollisionStay(Collision collision)
    {
        if (!photonView.IsMine)
            return;

        if (collision.gameObject.tag == "L04")
        {
           // Debug.Log("Tag Work");
            m_Rigidbody.constraints = RigidbodyConstraints.None;
            m_Rigidbody.constraints = RigidbodyConstraints.None;

        }
        
    }
    // Update is called once per frame
    void Update()
    {
      
        
    }

   
}
