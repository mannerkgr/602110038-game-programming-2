﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    public GameObject Laser_R;
    public GameObject Laser_B;
    public static bool disabled1 = false;
    public static bool disabled2 = false;

     /*void OnTriggerStay(Collider other)
    {
       //if (other.tag == "Box")
        //{
            Laser.SetActive(true);
        //}
            
        
    }*/
    /*void OnTriggerExit(Collider other)
    {
       

            Laser.SetActive(false);
        


    }*/
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(disabled1)
            Laser_R.SetActive (false);
        else
            Laser_R.SetActive (true);
        

       if(disabled2)
            Laser_B.SetActive (false);
        else
            Laser_B.SetActive (true);
        
    }
}
