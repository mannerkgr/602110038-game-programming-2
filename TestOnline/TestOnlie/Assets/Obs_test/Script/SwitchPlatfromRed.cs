﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPlatfromRed : MonoBehaviour
{
    public float speed = 1;
    private int direction = 1;

    public bool openswitch  ;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        if(openswitch){
        transform.Translate(Vector3.forward *speed*direction*Time.deltaTime);
        }
    }

     void OnTriggerEnter(Collider other) {
         
         
         
        if(other.tag == "Target"){
            
             if(direction == 1)
             direction = -1;
         else
         
             direction = 1;
         }

         if(other.tag == "L04"){
             other.transform.parent = transform;
         }
        
    }

    void OnTriggerStay(Collider other) {

        if(other.tag == "L04"){
             
             openswitch = true;
               

        }
        
    }



    void OnTriggerExit(Collider other) {
        if(other.tag == "L04"){
            other.transform.parent = null;
            openswitch = false;        
            }
        
    }
}

