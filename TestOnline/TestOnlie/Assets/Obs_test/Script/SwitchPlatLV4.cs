﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPlatLV4 : MonoBehaviour
{
    public GameObject Plat1;
    public GameObject Plat2;
    public GameObject Plat3;
    public GameObject Plat4;
    public static bool disabled1 = true;
    public static bool disabled2 = true;
    public static bool disabled3 = true;
    public static bool disabled4 = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(disabled1)
            Plat1.SetActive (false);
        else
            Plat1.SetActive (true);
        

       if(disabled2)
            Plat2.SetActive (false);
        else
            Plat2.SetActive (true);
        

        if(disabled3)
            Plat3.SetActive (false);
        else
            Plat3.SetActive (true);


       if(disabled4)
            Plat4.SetActive (false);
        else
            Plat4.SetActive (true);
    }
}
