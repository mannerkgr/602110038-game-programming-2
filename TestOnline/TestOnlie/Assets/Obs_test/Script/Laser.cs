﻿
﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {
    public Transform startPoint;
    public Transform endPoint;

    LineRenderer LaserLine;

   void Start() {
       LaserLine = GetComponent<LineRenderer>();
       LaserLine.SetWidth(0.2f,0.2f);

       
   }
   
    

    void Update() {
        LaserLine.SetPosition(0,startPoint.position);
        LaserLine.SetPosition(1,endPoint.position);
       
   }
 private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "L04")
        {
            Debug.Log("Player L04 have been hit by Laser");

        }
        if (other.gameObject.tag == "L38")
        {
            Debug.Log("Player L38 have been hit by Laser");

        }
    }
    
}
