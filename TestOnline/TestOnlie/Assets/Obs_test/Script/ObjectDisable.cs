﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDisable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerStay(Collider col) {


//LV2
        switch (col.gameObject.name){

            case "Switch_R":
                Switch.disabled1 = true;
                break;
            case "Enable_R":
                Switch.disabled1 = false;
                break;
        }


         switch (col.gameObject.name){

            case "Switch_B":
                Switch.disabled2 = true;
                break;
            case "Enable_B":
                Switch.disabled2 = false;
                break;
        }


//LV4
        switch (col.gameObject.name){

            case "Switch_Con1":
                SwitchPlatLV4.disabled1 = false;
                break;
            case "Enable_B":
                SwitchPlatLV4.disabled1 = false;
                break;
        }

        switch (col.gameObject.name){

            case "Switch_Con2":
                SwitchPlatLV4.disabled2 = false;
                break;
            case "Enable_B":
                SwitchPlatLV4.disabled2 = false;
                break;
        }
        switch (col.gameObject.name){

            case "Switch_Con3":
                SwitchPlatLV4.disabled3 = false;
                break;
            case "Enable_B":
                SwitchPlatLV4.disabled3 = false;
                break;
        }
        switch (col.gameObject.name){

            case "Switch_Con4":
                SwitchPlatLV4.disabled4 = false;
                break;
            case "Enable_B":
                SwitchPlatLV4.disabled4 = false;
                break;
        }
        
    }

  void OnTriggerExit(Collider col) {

      //LV2
        switch (col.gameObject.name){

            case "Switch_R":
                Switch.disabled1 = false;
                break;
            case "Enable_R":
                Switch.disabled1 = true;
                break;
        }
        



        switch (col.gameObject.name){

            case "Switch_B":
                Switch.disabled2 = false;
                break;
            case "Enable_B":
                Switch.disabled2 = true;
                break;
        }


        //LV4


        switch (col.gameObject.name){

            case "Switch_Con1":
                SwitchPlatLV4.disabled1 = true;
                break;
            case "Enable_B":
                SwitchPlatLV4.disabled1 = true;
                break;
        }
        switch (col.gameObject.name){

            case "Switch_Con2":
                SwitchPlatLV4.disabled2 = true;
                break;
            case "Enable_B":
                SwitchPlatLV4.disabled2 = true;
                break;
        }
        switch (col.gameObject.name){

            case "Switch_Con3":
                SwitchPlatLV4.disabled3 = true;
                break;
            case "Enable_B":
                SwitchPlatLV4.disabled3 = true;
                break;
        }
        switch (col.gameObject.name){

            case "Switch_Con4":
                SwitchPlatLV4.disabled4 = true;
                break;
            case "Enable_B":
                SwitchPlatLV4.disabled4 = true;
                break;
        }
    }


}

