﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerListEntry : MonoBehaviourPunCallbacks
{
    [Header("UI References")]
    public Text PlayerNameText;

    public Button PlayerColorButton;
    public Button PlayerReadyButton;
    public Image PlayerReadyImage;
    public Dropdown PlayerTeamDropdown; 

    private int ownerId;
    private bool isPlayerReady;
    private bool isTeamMode;
    #region UNITY

    public void Start()
    {
        if (isTeamMode)
        {
            PlayerTeamDropdown.gameObject.SetActive(true);
            Array enumVals = Enum.GetValues(typeof(PunTeams.Team));
            foreach (var enumVal in enumVals)
            {
                if((PunTeams.Team)enumVal != PunTeams.Team.none)
                    PlayerTeamDropdown.options.Add(new Dropdown.OptionData(enumVal.ToString()));
            }
                

            TeamExtensions.SetTeam(PhotonNetwork.LocalPlayer, (PunTeams.Team)(PlayerTeamDropdown.value + 1));
        }else
        {
            TeamExtensions.SetTeam(PhotonNetwork.LocalPlayer, PunTeams.Team.none);
        }

        if (PhotonNetwork.LocalPlayer.ActorNumber != ownerId)
        {
            PlayerReadyButton.gameObject.SetActive(false);
            PlayerTeamDropdown.interactable = false;
        }
        else
        {
            SettingPlayerProperties();

            PhotonNetwork.LocalPlayer.SetCustomProperties(new Hashtable()
            {
                { PunGameSetting.PLAYER_READY, isPlayerReady}
            });

            PlayerReadyButton.onClick.AddListener(() =>
            {
                isPlayerReady = !isPlayerReady;
                SetPlayerReady(isPlayerReady);

                PhotonNetwork.LocalPlayer.SetCustomProperties(new Hashtable()
                {
                    { PunGameSetting.PLAYER_READY, isPlayerReady}
                });

                if (PhotonNetwork.IsMasterClient)
                {
                    FindObjectOfType<InsideRoomPanel>().LocalPlayerPropertiesUpdated();
                }
            });

            PlayerColorButton.onClick.AddListener(() =>
            {
                PhotonNetwork.LocalPlayer.SetCustomProperties(new Hashtable
                {
                    { PunGameSetting.PLAYER_COLOR, UnityEngine.Random.Range(0,7) }
                });
            });

            PlayerTeamDropdown.onValueChanged.AddListener((int currentValue) =>
            {
                int plusValue = currentValue + 1;
                print("Change Value : " + plusValue);

                TeamExtensions.SetTeam(PhotonNetwork.LocalPlayer, (PunTeams.Team)(plusValue));
            });
        }
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PlayerNumbering.OnPlayerNumberingChanged += OnPlayerNumberingChanged;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PlayerNumbering.OnPlayerNumberingChanged -= OnPlayerNumberingChanged;
    }

    #endregion

    public void Initialize(int playerId, string playerName, bool isTeamMode)
    {
        ownerId = playerId;
        PlayerNameText.text = playerName;
        this.isTeamMode = isTeamMode;
    }

    public static void SettingPlayerProperties()
    {
        PhotonNetwork.LocalPlayer.SetCustomProperties(new Hashtable()
        {
            { PunGameSetting.PLAYER_LIVES, PunGameSetting.PLAYER_MAX_LIVES }
        });
        PhotonNetwork.LocalPlayer.SetScore(0);
    }

    /// <summary>
    /// Setting FirstTime when Player Joined Room
    /// </summary>
    private void OnPlayerNumberingChanged()
    {
        foreach (Player p in PhotonNetwork.PlayerList)
        {
            if (p.ActorNumber == ownerId)
            {
                PlayerColorButton.GetComponent<Image>().color = PunGameSetting.GetColor(p.GetPlayerNumber());
            }
        }
    }

    public void SetPlayerReady(bool playerReady)
    {
        PlayerReadyButton.GetComponentInChildren<Text>().text = playerReady ? "Ready!" : "Ready?";
        PlayerReadyImage.enabled = playerReady;
        PlayerColorButton.interactable = !playerReady;
    }

    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);

        if (changedProps.ContainsKey(PunGameSetting.PLAYER_COLOR) &&
            target.ActorNumber == ownerId)
        {
            object colors;
            if (changedProps.TryGetValue(PunGameSetting.PLAYER_COLOR, out colors))
            {
                PlayerColorButton.GetComponent<Image>().color = PunGameSetting.GetColor((int)colors);
            }

            return;
        }

        if (changedProps.ContainsKey(PunTeams.TeamPlayerProp) &&
            target.ActorNumber == ownerId)
        {
            int currentTeam = (int)TeamExtensions.GetTeam(target);
            print("Current Team: " + currentTeam);
            PlayerTeamDropdown.value = currentTeam - 1;
            return;
        }
    }
}
