﻿using UnityEngine;

public class PunGameSetting
{
    public const float MIN_SPAWN_TIME = 5.0f;
    public const float MAX_SPAWN_TIME = 10.0f;

    public const float PLAYER_RESPAWN_TIME = 4.0f;

    public const string PLAYER_LIVES = "PlayerLives";
    public const int PLAYER_MAX_LIVES = 80;

    
    public const string PLAYER_READY = "IsPlayerReady";
    public const string PLAYER_LOADED_LEVEL = "PlayerLoadedLevel";
    public const string PLAYER_COLOR = "PlayerColor";

    public const string CountdownStartTime = "GameTime";
    public const string GAMEOVER = "IsGameOver";

    public const string TEAMMODE = "TeamMode";

    public static Color GetColor(int colorChoice)
    {
        switch (colorChoice)
        {
            // Update index becuase of re index to PunTeams.cs
            case 0: return Color.white;
            case 1: return Color.red;
            case 2: return Color.blue;
            //
            case 3: return Color.green;
            case 4: return Color.yellow;
            case 5: return Color.cyan;
            case 6: return Color.grey;
            case 7: return Color.magenta;
            
        }

        return Color.black;
    }
}
