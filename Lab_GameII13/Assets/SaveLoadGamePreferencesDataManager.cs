﻿using System.IO;
 using UnityEngine;
 using UnityEngine.UI;

 public class SaveLoadGamePreferencesDataManager : MonoBehaviour
 {
[SerializeField]
 private InputField _inputFieldPlayerName, _inputFieldScore, _inputFieldFileName, _InputFieldClan;
 [SerializeField]
 private Button _saveButton, _loadButton;
    [SerializeField]
 private Slider _sliderVitality, _sliderMagic, _sliderAgility;
 [SerializeField]
 Dropdown _dropdownSaveType, _dropdownRace;

 private string _saveFolderLocation;

 // Start is called before the first frame update
 void Start()
 {
 _saveButton.onClick.AddListener(OnSaveClick);
 _loadButton.onClick.AddListener(OnLoadClick);


 if (!Directory.Exists("Saves"))
 Directory.CreateDirectory("Saves");
 _saveFolderLocation = "Saves";
 }

 // Update is called once per frame
 void Update()
 {

 }

 void OnSaveClick()
{
        GamePreferencesData gdp = new GamePreferencesData
        {
            _playerName = _inputFieldPlayerName.text,
            _score = _inputFieldScore.text,
            _clan = _InputFieldClan.text,
            _Vitality = _sliderVitality.value,
            _Magic = _sliderMagic.value,
            _Agility = _sliderAgility.value,
            _Race = _dropdownRace.options[_dropdownRace.value].text
     };
    
 string filename = _inputFieldFileName.text;
     string location = Path.Combine(_saveFolderLocation, filename);
    
 ISaveLoadGamePreferencesData islg = null;
    
 switch (_dropdownSaveType.value) {
        case 0://XML
             islg = new SaveLoadGamePreferencesDataXML();
             location = location + ".xml";
             break;
             case 1://JSON
             islg = new SaveLoadGamePreferencesDataJSON();
             location = location + ".json";
             break;
             case 2://Binary
             islg = new SaveLoadGamePreferencesDataBinary();
             location = location + ".bin";
             break;
             }
    
 islg.SaveGamePreferencesData(gdp, location);
 }

 void OnLoadClick()
 {
 string filename = _inputFieldFileName.text;
 string location = Path.Combine(_saveFolderLocation, filename);

 ISaveLoadGamePreferencesData islg = null;

 switch (_dropdownSaveType.value)
 {
 case 0://XML
 islg = new SaveLoadGamePreferencesDataXML();
 location = location + ".xml";
 break;
 case 1://JSON
 islg = new SaveLoadGamePreferencesDataJSON();
 location = location + ".json";
 break;
 case 2://Binary
 islg = new SaveLoadGamePreferencesDataBinary();
 location = location + ".bin";
 break;
 }
    GamePreferencesData gdp = new GamePreferencesData();
 gdp = islg.LoadGamePreferencesData(location);

 _inputFieldPlayerName.text = gdp._playerName;
 _inputFieldScore.text = gdp._score;
        _InputFieldClan.text = gdp._clan;
 _sliderVitality.value = gdp._Vitality;
 _sliderMagic.value = gdp._Magic;
 _sliderAgility.value = gdp._Agility;
        _dropdownRace.options[_dropdownRace.value].text = gdp._Race;
    }
 }