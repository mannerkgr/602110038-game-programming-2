﻿
using System;

[Serializable]
public class GamePreferencesData 
{
    public string _playerName;
    public string _score;
    public string _clan;
    public float _Vitality;
    public float _Magic;
    public float _Agility;
    public string _Race;
}
