﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Pattern_Movement
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;


        Texture2D mouseTexture;
        Vector2 mousePosition;

        Viewport vp;
        Random random;

        int counter = 0;

        ControlData[] Pattern;

        int CurrentIndex=0;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferHeight = 768;
            graphics.PreferredBackBufferWidth = 1024;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            vp = graphics.GraphicsDevice.Viewport;

            random = new Random();

           
            mousePosition = new Vector2(400, 400);

            Pattern = new ControlData[4];

            for (int i = 0; i < Pattern.Length; i++)
                Pattern[i] = new ControlData();

            Pattern[0].turnRight = 0;
            Pattern[0].turnLeft = 0;
            Pattern[0].stepForward = 50;
            Pattern[0].stepBackward = 0;
            Pattern[1].turnRight = 0;
            Pattern[1].turnLeft = 50;
            Pattern[1].stepForward = 0;
            Pattern[1].stepBackward = 0;
            Pattern[2].turnRight = 0;
            Pattern[2].turnLeft = 0;
            Pattern[2].stepForward = 0;
            Pattern[2].stepBackward = 50;
            Pattern[3].turnRight = 50;
            Pattern[3].turnLeft = 0;
            Pattern[3].stepForward = 0;
            Pattern[3].stepBackward = 0;



            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>("myFont");
            mouseTexture = Content.Load<Texture2D>("mouse");
           
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            
            // TODO: Add your update logic here
           
            doBasicPattern();

           


            counter++;
            if (counter > 59)
                counter = 0;
            base.Update(gameTime);
        }

        private void doBasicPattern()
        {
            if (counter % 30 == 0)
            {
                mousePosition.X +=  Pattern[CurrentIndex].turnRight ;
                mousePosition.X -=  Pattern[CurrentIndex].turnLeft ;
                mousePosition.Y +=Pattern[CurrentIndex].stepForward ;
                mousePosition.Y -= Pattern[CurrentIndex].stepBackward;
                CurrentIndex++;
                if (CurrentIndex >= Pattern.Length)
                    CurrentIndex = 0;
            }
                

        }

        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();


            spriteBatch.Draw(mouseTexture, mousePosition , null, Color.White, 0, Vector2.Zero, 0.5f, 0, 0);

            spriteBatch.DrawString(spriteFont, "Mouse pos X,y" + mousePosition + "Current Index " + CurrentIndex, new Vector2(200, 40), Color.White);

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
    class ControlData {
        public float turnRight;
        public float turnLeft;
        public float stepForward;
        public float stepBackward;
    }


}
