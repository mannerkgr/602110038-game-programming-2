﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Continuous_Line_of_sight_Chasing
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        Texture2D arrowTexture;
        Vector2 arrowPosition;
        float arrowRotation = 0.0f;
        float arrow_speed = 1.0f;

        Texture2D mouseTexture;
        Vector2 mousePosition;
        private Vector2 mouseTextureCenter;

        Viewport vp;
        Random random;
        KeyboardState old_ks;
        private Vector2 arrowTextureCenter;
        private Vector2 arrow_Direction;
        private float facingDirection;

        // ZigZag 5 
        const int kMaxpattern = 10;
        private float[,] pattern = new float[kMaxpattern, 3];
        private int currentIndex=0;
        private Vector2 cumulative_distance = new Vector2(0,0);
        private bool stop = false;
        private Vector2 calTarget;
        private float angleTarget=0;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferHeight = 768;
            graphics.PreferredBackBufferWidth = 1024;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            vp = graphics.GraphicsDevice.Viewport;
            random = new Random();

            arrowPosition = new Vector2(50, 300);
            mousePosition = new Vector2(random.Next(vp.Width), random.Next(vp.Height));

            // ZigZag
          //  setPattern(1.0f, 20, 0, 0);
           // setPattern(1.0f, 200, 45.0f, 1);
           // setPattern(1.0f, 200, -90.0f, 2);
            //setPattern(1.0f, 200, 90.0f, 3);
           // setPattern(1.0f, 200, -90.0f, 4);

            //Square
            //setPattern(1.0f, 200, 0, 0);
           // setPattern(1.0f, 200, 90.0f, 1);
           // setPattern(1.0f, 200, 90.0f, 2);
           // setPattern(1.0f, 200, 90.0f, 3);
           // setPattern(1.0f, 20, 90.0f, 4);

            //Arbitrary
            setPattern(1.0f, 200, 0, 0);
            setPattern(1.0f, 200, 110f, 1);
            setPattern(1.0f, 100, -90.0f, 2);
            setPattern(1.0f, 200, -20.0f, 3);
            setPattern(1.0f, 300, -70.0f, 4);
            setPattern(1.0f, 200, -70.0f, 5);
            setPattern(1.0f, 200, -45.0f, 6);
            setPattern(1.0f, 200, -45.0f, 7);


            angleTarget = angleTarget + pattern[currentIndex, 2]+arrowRotation;
            calTarget = new Vector2(
                (float)Math.Cos(MathHelper.ToRadians(pattern[currentIndex, 2])) * pattern[currentIndex, 1],
                (float)Math.Sin(MathHelper.ToRadians(pattern[currentIndex, 2])) * pattern[currentIndex, 1]) + arrowPosition;

            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>("myFont");
            arrowTexture = Content.Load<Texture2D>("arrow_up");
            mouseTexture = Content.Load<Texture2D>("mouse");
            // TODO: use this.Content to load your game content here

            arrowTextureCenter =
                new Vector2(arrowTexture.Width / 2, arrowTexture.Height / 2);
            mouseTextureCenter =
                new Vector2(mouseTexture.Width / 2, mouseTexture.Height / 2);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (old_ks.IsKeyDown(Keys.D1) && Keyboard.GetState().IsKeyUp(Keys.D1))
            {
                mousePosition = new Vector2(random.Next(vp.Width), random.Next(vp.Height));
                old_ks = Keyboard.GetState();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D1))
            {
                old_ks = Keyboard.GetState();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                arrowRotation -= 1;
                arrowRotation = arrowRotation % 360;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                arrowRotation += 1;
                arrowRotation = arrowRotation % 360;
            }
            if (old_ks.IsKeyDown(Keys.Up) && Keyboard.GetState().IsKeyUp(Keys.Up))
            {
                arrow_speed += 0.5f;
                
            }
            if (old_ks.IsKeyDown(Keys.Down) && Keyboard.GetState().IsKeyUp(Keys.Down))
            {
                arrow_speed -= 0.5f;

            }
            old_ks = Keyboard.GetState();
            // TODO: Add your update logic here

            if(stop == false)
            DoPatternMovementSimulatedEnvir();

            // Random positions if objects are Out of viewport
            if (mousePosition.X < 0 || mousePosition.X > vp.Width)
                mousePosition = new Vector2(random.Next(vp.Width), random.Next(vp.Height));
            if (mousePosition.Y < 0 || mousePosition.Y > vp.Width)
                mousePosition = new Vector2(random.Next(vp.Width), random.Next(vp.Height));
            if (arrowPosition.X < 0 || arrowPosition.X > vp.Width)
                arrowPosition = new Vector2(random.Next(vp.Width), random.Next(vp.Height));
            if (arrowPosition.Y < 0 || arrowPosition.Y > vp.Width)
                arrowPosition = new Vector2(random.Next(vp.Width), random.Next(vp.Height));

            base.Update(gameTime);
        }

        void setPattern(float inspeed, float indistant, float inrotation, int index)
        {
            pattern[index, 0] = inspeed;
            pattern[index, 1] = indistant;
            pattern[index, 2] = inrotation;

        }
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

           
            spriteBatch.Draw(arrowTexture, arrowPosition, null, Color.White, MathHelper.ToRadians(arrowRotation+90), arrowTextureCenter , 1.0f, 0, 0);

            spriteBatch.DrawString(spriteFont, "Arrow Direction" + "    Angle " + arrowRotation + "     Direction " + facingDirection, new Vector2(200, 20), Color.White);
            spriteBatch.DrawString(spriteFont, "Pattern idex " + currentIndex + "   speed " + pattern[currentIndex, 0] + "  distance " + pattern[currentIndex, 1] + "   rotation " + pattern[currentIndex,2], new Vector2(200, 40), Color.White);
            spriteBatch.DrawString(spriteFont, "cumulative distance "+ cumulative_distance.Length() , new Vector2(200, 60), Color.White);
       

            spriteBatch.End();
            base.Draw(gameTime);
        }

        void DoPatternMovementSimulatedEnvir()
        {

            Vector2 distance = calTarget - arrowPosition;

            float angle_offset = 3;
              
           if (angleTarget > arrowRotation)
            {
                if (angle_offset > Math.Abs(angleTarget - arrowRotation))
                    arrowRotation += Math.Abs(angleTarget - arrowRotation);
                else
                    arrowRotation += angle_offset;
                arrowRotation = arrowRotation % 360;

            }
            else if (angleTarget < arrowRotation)
            {
                if (angle_offset > Math.Abs(angleTarget - arrowRotation))
                    arrowRotation -= Math.Abs(angleTarget - arrowRotation);
                else
                    arrowRotation -= angle_offset;
                arrowRotation = arrowRotation % 360;
            }

            arrow_Direction = new Vector2(
                (float)Math.Cos(MathHelper.ToRadians(arrowRotation)) * arrow_speed,
                (float)Math.Sin(MathHelper.ToRadians(arrowRotation)) * arrow_speed);
            float temp = cumulative_distance.Length();

            if (cumulative_distance.Length() < pattern[currentIndex, 1])
            {
                arrowPosition += arrow_Direction;
                cumulative_distance += arrow_Direction;
            }
            else
            {

                    if (currentIndex < kMaxpattern - 1)
                    {
                        currentIndex++;
                        cumulative_distance = new Vector2(0, 0);

                        angleTarget = pattern[currentIndex, 2] + arrowRotation;
                        calTarget = new Vector2(
                    (float)Math.Cos(MathHelper.ToRadians(angleTarget)) * pattern[currentIndex, 1],
                    (float)Math.Sin(MathHelper.ToRadians(angleTarget)) * pattern[currentIndex, 1]) + arrowPosition;
                    }
                    else
                    {
                        
                        stop = true;
                    }

            }
                   
        }

    }
}
