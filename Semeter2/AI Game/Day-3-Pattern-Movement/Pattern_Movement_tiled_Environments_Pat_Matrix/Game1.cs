﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Pattern_Movement_tiled_Environments
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        Texture2D catTexture;
        Texture2D bgTexture;

        public static Vector2 catPosition;

        Texture2D mouseTexture;
        Vector2 mousePosition;


        Texture2D tiledTexture;
        Viewport vp;
        Random random;
        KeyboardState old_ks;
        int counter = 0;

        double[,] scores = new double[20, 20];

        bool stop = false;

        public static int currentStep = 0;

        public int kMaxPathLength = 7;
        float[] pathRow;
        float[] pathCol;
        const int kMaxRows = 20;
        const int kMaxCols = 20;
        float[,] pattern = new float[kMaxRows, kMaxCols];
        float[,] markedpattern = new float[kMaxRows, kMaxCols];
        float patternIndexCol = 0, patternIndexRow = 0;
        bool patterStop = false;
       
        int previousRow = 0, previousCol = 0;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferHeight = 768;
            graphics.PreferredBackBufferWidth = 1024;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            vp = graphics.GraphicsDevice.Viewport;


            random = new Random();

            catPosition = new Vector2(random.Next(10), random.Next(10));
            mousePosition = new Vector2(random.Next(10), random.Next(10));

                pathRow = new float[kMaxPathLength];
                pathCol = new float[kMaxPathLength];

                for (int i = 0; i < kMaxPathLength; i++)
                {
                    pathRow[i] = -1;
                    pathCol[i] = -1;
                }

            BuildPatternSegment(3, 2, 16, 2,0);
            BuildPatternSegment(16, 2, 16, 11,1);
            BuildPatternSegment(16, 11, 9, 11,2);
            BuildPatternSegment(9, 11, 9, 2,3);
            BuildPatternSegment(9, 2, 9, 6, 4);
            BuildPatternSegment(9, 6, 3, 6,5);
            BuildPatternSegment(3, 6, 3, 2,6);

            catPosition.X = pathCol[0];
            catPosition.Y = pathRow[0];

            pattern[(int)pathCol[0], (int)pathRow[0]] = 1;
            patternIndexCol = pathCol[0];
            patternIndexRow = pathRow[0];
            for (int i = 0; i < kMaxRows; i++)
                for (int j = 0; j < kMaxCols; j++)
                    pattern[i,j] = 0;

            while (!patterStop) {
                doPatternSegment();
            }
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>("myFont");
            catTexture = Content.Load<Texture2D>("cat");
            mouseTexture = Content.Load<Texture2D>("mouse");
            tiledTexture = Content.Load<Texture2D>("square");
            bgTexture = Content.Load<Texture2D>("bg");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (old_ks.IsKeyDown(Keys.D1) && Keyboard.GetState().IsKeyUp(Keys.D1))
            {
                mousePosition = new Vector2(random.Next(10), random.Next(10));
                stop = false;
                old_ks = Keyboard.GetState();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D1))
            {
                old_ks = Keyboard.GetState();
            }
            // TODO: Add your update logic here

            if (counter % 30 == 0 && stop == false)
            {
                FollowPattern();
            }

            counter++;
            if (counter > 59)
                counter = 0;
            base.Update(gameTime);
        }

        void BuildPathSegment(float startRow, float startCol, float endRow, float endCol, int index)
        {

            if (index == 0)
            {
                pathCol[index] = startCol;
                pathRow[index] = startRow;
                pathCol[index+1] = endCol- startCol;
                pathRow[index+1] = endRow- startRow;

            }
            else if(index <pathCol.Length-1)
            {
                pathCol[index+1] = endCol - startCol;
                pathRow[index+1] = endRow - startRow;

            }
        }


        void BuildPatternSegment(float startRow, float startCol, float endRow, float endCol, int index)
        {

            if (index == 0)
            {
                pathCol[index] = startCol;
                pathRow[index] = startRow;
                pathCol[index + 1] = endCol - startCol;
                pathRow[index + 1] = endRow - startRow;

            }
            else if (index < pathCol.Length - 1)
            {
                pathCol[index + 1] = endCol - startCol;
                pathRow[index + 1] = endRow - startRow;

            }
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            for (int i = 0; i < kMaxCols; i++)
                for (int j = 0; j < kMaxRows; j++)
                {
                    if(pattern[j,i] == 1)
                    spriteBatch.Draw(tiledTexture, new Vector2(i * 32, j * 32), null, Color.White, 0, Vector2.Zero, 0.25f, 0, 0);
                }
           
            spriteBatch.Draw(catTexture, catPosition * 32, null, Color.White, 0, Vector2.Zero, 0.25f, 0, 0);
            spriteBatch.DrawString(spriteFont, "Cat pos X,y" + catPosition, new Vector2(800, 20), Color.White);
            spriteBatch.DrawString(spriteFont, "Mouse pos X,y" + mousePosition, new Vector2(800, 40), Color.White);

            spriteBatch.End();
            base.Draw(gameTime);
        }


        private void doPatternSegment()
        {

            float endCol;
            float endRow;
            float sumCol = 0, sumRow = 0;

            
            if (currentStep == pathCol.Length - 1)
            {
                endCol = pathCol[0];
                endRow = pathRow[0];
            }
            else
            {
                for (int i = 0; i <= currentStep; i++)
                {
                    sumCol += pathCol[i];
                    sumRow += pathRow[i];
                }
                endCol = sumCol + pathCol[currentStep + 1];
                endRow = sumRow + pathRow[currentStep + 1];
            }

            float nextCol = patternIndexCol;
            float nextRow = patternIndexRow;
            float deltaRow = endRow - patternIndexRow;
            float deltaCol = endCol - patternIndexCol;
            float stepCol = 1, stepRow = 1;
            float fraction;


            if (deltaRow < 0) stepRow = -1; else stepRow = 1;
            if (deltaCol < 0) stepCol = -1; else stepCol = 1;

            deltaRow = Math.Abs(deltaRow);
            deltaCol = Math.Abs(deltaCol);

            if (deltaCol > deltaRow)
            {
                fraction = deltaRow * 2 - deltaCol;

                if (fraction >= 0 && nextCol != endCol)
                {
                    nextRow = nextRow + stepRow;

                }
                nextCol = nextCol + stepCol;

                patternIndexCol = nextCol;
                patternIndexRow = nextRow;
                pattern[(int)patternIndexRow, (int)patternIndexCol] = 1;
            }
            else
            {
                fraction = deltaCol * 2 - deltaRow;

                if (fraction >= 0 && nextRow != endRow)
                {
                    nextCol = nextCol + stepCol;
                }
                nextRow = nextRow + stepRow;

                patternIndexCol = nextCol;
                patternIndexRow = nextRow;
                pattern[(int)patternIndexRow, (int)patternIndexCol] = 1;
            }
            if (patternIndexCol == endCol && patternIndexRow == endRow)
            {
                currentStep++;
                if (currentStep >= kMaxPathLength)
                {
                    currentStep = 0;
                    patterStop = true;
                }
            }

        }

        private void FollowPattern()
        {
            int i, j, randstep = 0 ;
            int row = (int)catPosition.Y, col = (int)catPosition.X;

            //const int total_direction = 4;
            //int[] possibleRowPath = new int[total_direction] { 0, 0, 0, 0};
            //int[] possibleColPath = new int[total_direction] { 0, 0, 0, 0};
            //int[] rowOffset = new int[total_direction] { -1, 0, 0, 1};
            //int[] colOffset = new int[total_direction] {  0, -1, 1, 0};

            const int total_direction = 8;
            int[] possibleRowPath = new int[total_direction] { 0, 0, 0, 0, 0, 0, 0, 0 };
            int[] possibleColPath = new int[total_direction] { 0, 0, 0, 0, 0, 0, 0, 0 };
            int[] rowOffset = new int[total_direction] { -1, -1, -1, 0, 0, 1, 1, 1 };
            int[] colOffset = new int[total_direction] { -1, 0, 1, -1, 1, -1, 0, 1 };

            j = 0;
            for (i = 0; i < total_direction; i++)
                if (pattern[row + rowOffset[i], col + colOffset[i]] == 1)
                    if (!(((row + rowOffset[i]) == previousRow) &&
                        ((col + colOffset[i]) == previousCol)))
                    {
                        possibleRowPath[j] = row + rowOffset[i];
                        possibleColPath[j] = col + colOffset[i];
                        j++;
                    }

            do
            {
                randstep = random.Next(0, 1000000000);
                randstep = randstep % (j);
                previousRow = row;
                previousCol = col;
                row = possibleRowPath[randstep];
                col = possibleColPath[randstep];
           
            } while (previousRow == row && previousCol == col);
  
            catPosition.X = col;
            catPosition.Y = row;

        }

    }
   
}
