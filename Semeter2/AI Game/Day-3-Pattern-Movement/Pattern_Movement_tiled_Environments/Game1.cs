﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Pattern_Movement_tiled_Environments
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        Texture2D catTexture;

        public static Vector2 catPosition;

        Texture2D mouseTexture;
        Vector2 mousePosition;


        Texture2D tiledTexture;
        Viewport vp;
        Random random;
        KeyboardState old_ks;
        int counter = 0;

        double[,] scores = new double[20, 20];

        bool stop = false;

        public static int currentStep = 0;

        public int kMaxPathLength = 4;
        float[] pathRow;
        float[] pathCol;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferHeight = 768;
            graphics.PreferredBackBufferWidth = 1024;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            vp = graphics.GraphicsDevice.Viewport;


            random = new Random();

            catPosition = new Vector2(random.Next(10), random.Next(10));
            mousePosition = new Vector2(random.Next(10), random.Next(10));

                pathRow = new float[kMaxPathLength];
                pathCol = new float[kMaxPathLength];

                for (int i = 0; i < kMaxPathLength; i++)
                {
                    pathRow[i] = -1;
                    pathCol[i] = -1;
                }

            BuildPathSegment(10, 3, 18, 3,0);
            BuildPathSegment(18, 3, 18, 12,1);
            BuildPathSegment(18, 12, 10, 12,2);
            BuildPathSegment(10, 12, 10, 3,3);

          
            catPosition.X = pathCol[0];
            catPosition.Y = pathRow[0];
               

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>("myFont");
            catTexture = Content.Load<Texture2D>("cat");
            mouseTexture = Content.Load<Texture2D>("mouse");
            tiledTexture = Content.Load<Texture2D>("square");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (old_ks.IsKeyDown(Keys.D1) && Keyboard.GetState().IsKeyUp(Keys.D1))
            {
                mousePosition = new Vector2(random.Next(10), random.Next(10));
                stop = false;
                old_ks = Keyboard.GetState();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D1))
            {
                old_ks = Keyboard.GetState();
            }
            // TODO: Add your update logic here

            if (counter % 30 == 0 && stop == false)
            {
               
               doPatternMovementTiled();

            }
            
            counter++;
            if (counter > 59)
                counter = 0;
            base.Update(gameTime);
        }

        void BuildPathSegment(float startRow, float startCol, float endRow, float endCol, int index)
        {
            pathCol[index] = startCol;
            pathRow[index] = startRow;
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            for (int i = 0; i < 20; i++)
                for (int j = 0; j < 20; j++)
                {
                    spriteBatch.Draw(tiledTexture, new Vector2(i * 32, j * 32), null, Color.White, 0, Vector2.Zero, 0.25f, 0, 0);
                }

            spriteBatch.Draw(mouseTexture, mousePosition * 32, null, Color.White, 0, Vector2.Zero, 0.25f, 0, 0);
            spriteBatch.Draw(catTexture, catPosition * 32, null, Color.White, 0, Vector2.Zero, 0.25f, 0, 0);
            spriteBatch.DrawString(spriteFont, "Cat pos X,y" + catPosition, new Vector2(800, 20), Color.White);
            spriteBatch.DrawString(spriteFont, "Mouse pos X,y" + mousePosition, new Vector2(800, 40), Color.White);



            spriteBatch.End();
            base.Draw(gameTime);
        }
        private void doPatternMovementTiled()
        {

            float endCol ;
            float endRow ;

            if(currentStep == pathCol.Length - 1)
            {
                endCol = pathCol[0];
                endRow = pathRow[0];
            }
            else
            {
                endCol = pathCol[currentStep + 1];
                endRow = pathRow[currentStep + 1];
            }

            float nextCol = catPosition.X;
            float nextRow = catPosition.Y;
            float deltaRow = endRow - catPosition.Y;
            float deltaCol = endCol - catPosition.X;
            float stepCol = 1, stepRow = 1;
            float fraction;


            if (deltaRow < 0) stepRow = -1; else stepRow = 1;
            if (deltaCol < 0) stepCol = -1; else stepCol = 1;

            deltaRow = Math.Abs(deltaRow);
            deltaCol = Math.Abs(deltaCol);

            if (deltaCol > deltaRow)
            {
                fraction = deltaRow * 2 - deltaCol;

                if (fraction >= 0 && nextCol != endCol)
                {
                    nextRow = nextRow + stepRow;

                }
                nextCol = nextCol + stepCol;

                catPosition.X = nextCol;
                catPosition.Y = nextRow;
            }
            else
            {
                fraction = deltaCol * 2 - deltaRow;

                if (fraction >= 0 && nextRow != endRow)
                {
                    nextCol = nextCol + stepCol;
                }
                nextRow = nextRow + stepRow;

                catPosition.X = nextCol;
                catPosition.Y = nextRow;
            }
            if(catPosition.X == endCol && catPosition.Y == endRow)
            {
                currentStep++;
                if(currentStep >= kMaxPathLength)
                {
                    currentStep = 0;
                }
            }
                
        }
        
    }
    
}
