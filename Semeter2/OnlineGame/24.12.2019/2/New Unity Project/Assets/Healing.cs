﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class Healing : MonoBehaviourPun
{
    public int healingPoint = 1;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Test other : " + other.gameObject.name);
        if (other.gameObject.CompareTag("Player"))
        {
            PunHealth otherHeal = other.gameObject.GetComponent<PunHealth>();
            otherHeal.Healing(healingPoint);
            photonView.RPC("PunRPCHealing", RpcTarget.MasterClient);
        }
    }
    [PunRPC]
    private void PunRPCHealing()
    {
        Destroy(this.gameObject);
    }
    private void OnDestroy()
    {
        if (!photonView.IsMine)
            return;
        PhotonNetwork.Destroy(this.gameObject);
    }
}
