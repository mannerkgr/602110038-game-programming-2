﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using Photon.Pun;
[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks,
IPunInstantiateMagicCallback
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;
    private void Awake()
    {
        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
// if (photonView.IsMine)
    //    {
    //        LocalPlayerInstance = gameObject;
     //   }
      //  else
     //   {
        //    GetComponentInChildren<Camera>().enabled = false;
        //   GetComponent<FirstPersonController>().enabled = false;
     //  }
    }
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {

        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());

        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
 if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
            GetComponent<MeshRenderer>().material.color = Color.blue;
        }
        else
        {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponentInChildren<AudioListener>().enabled = false;
            GetComponent<FirstPersonController>().enabled = false;
        }
    }
}
