﻿using UnityEngine;
using Photon.Pun;
public class PunHealth : MonoBehaviourPunCallbacks, IPunObservable
{
    public const int maxHealth = 100;
    public int currentHealth = maxHealth;
    public void OnGUI()
    {
        if (photonView.IsMine)
            GUI.Label(new Rect(0, 0, 300, 50), "Player Health : " + currentHealth);
    }
    public void TakeDamage(int amount, int OwnerNetID)
    {
        if (photonView != null)
            photonView.RPC("PunRPCTakedDamage", RpcTarget.All, amount, OwnerNetID);
        else print("photonView is NULL.");
    }
    [PunRPC]
    public void PunRPCTakedDamage(int amount, int OwnerNetID)
    {
        Debug.Log("Take Damage");
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            Debug.Log("BulletID : " + OwnerNetID.ToString() + " Killed" +
           photonView.ViewID);
            currentHealth = maxHealth;
        }
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(currentHealth);
        }
        else
        {
            currentHealth = (int)stream.ReceiveNext();
        }
    }
    public void Healing(int amout)
    {
        currentHealth += amout;
    }
}