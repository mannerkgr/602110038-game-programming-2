﻿using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
public class PunNetworkManager : ConnectAndJoinRandom
{
    bool GameStart = false;
    bool isFirstSetting = false;
    public GameObject HealingPrefab;
    public int numberOfHealing = 5;
    float m_count = 0;
    public float m_CountDownDropHeal = 10;

    public static PunNetworkManager singleton;
    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;
    private void Awake()
    {
        singleton = this;
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Camera.main.gameObject.SetActive(false);
        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer();
        }
        else {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }
    }
    public void SpawnPlayer()
    {
        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
    PhotonNetwork.Instantiate(GamePlayerPrefab.name,
 new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
        GameStart = true;
    }
    private void AirDropHealing()
    {
        if (isFirstSetting == false)
        {
            isFirstSetting = true;
            int half = numberOfHealing / 2;
            for (int i = 0; i < half; i++)
            {

                PhotonNetwork.InstantiateSceneObject(HealingPrefab.name
                 , AirDrop.RandomPosition(5f)
                 , AirDrop.RandomRotation()
                 , 0);
            }
            m_count = m_CountDownDropHeal;
        }
        else
        {
            if (GameObject.FindGameObjectsWithTag("Healing").Length < numberOfHealing)
            {
                m_count -= Time.deltaTime;
                if (m_count <= 0)
                {
                    m_count = m_CountDownDropHeal;
                    PhotonNetwork.Instantiate(HealingPrefab.name
                    , AirDrop.RandomPosition(10f)
                    , AirDrop.RandomRotation()
                    , 0);
                }
            }
        }
    }
    private void Update()
    {
        if (PhotonNetwork.IsMasterClient != true)
            return;
        if (GameStart == true)
        {
            AirDropHealing();
        }
    }
}
