﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SamplePlatformer : MonoBehaviour
{
    [SerializeField, Tooltip("Max speed, in units per second, that the character moves.")]
    float speed = 9;

    [SerializeField, Tooltip("Acceleration while grounded.")]
    float walkAcceleration = 75;

    [SerializeField, Tooltip("Acceleration while in the air.")]
    float airAcceleration = 30;

    [SerializeField, Tooltip("Deceleration applied when character is grounded and not attempting to move.")]
    float groundDeceleration = 70;

    [SerializeField, Tooltip("Max height the character will jump regardless of gravity")]
    float jumpHeight = 4;

    private Vector2 velocity;
    private BoxCollider2D boxCollider;
    private bool grounded ;
    public GameObject myExplosion;

    private Animator animator;
    private SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }


    void Start()
    {
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float moveInput = Input.GetAxisRaw("Horizontal");

           if (grounded)
        {
            velocity.y = 0;

            if (Input.GetButtonDown("Jump"))
            {
                // Calculate the velocity required to achieve the target jump height.
                velocity.y = Mathf.Sqrt(2 * jumpHeight * Mathf.Abs(Physics2D.gravity.y));

                animator.SetTrigger("TAKEOFF");
            }
        }

           if(Input.GetButtonDown("Fire1"))
        {
            animator.SetBool("ISATTACK", true);
            Instantiate(myExplosion, transform.position + new Vector3(1.5f, 0, 0), Quaternion.identity);
        }
           if(animator.GetBool("ISATTACK") == true)
        {
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
                    animator.SetBool("ISATTACK", false);
            }
        }

        float acceleration = grounded ? walkAcceleration : airAcceleration;
        float deceleration = grounded ? groundDeceleration : 0;

     

        if (moveInput != 0)
        {
            velocity.x = Mathf.MoveTowards(velocity.x, speed * moveInput, acceleration * Time.deltaTime);
        }
        else
        {
            velocity.x = Mathf.MoveTowards(velocity.x, 0, deceleration * Time.deltaTime);
         }


        bool flipSprite = (spriteRenderer.flipX ? (velocity.x > 0.01f) : (velocity.x < 0f));

        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;

        }


        if (grounded == true)
        {
            animator.SetBool("ISJUMP", false);
        }
        else
        {
            animator.SetBool("ISJUMP", true);
        }

        if(velocity.x != 0)
        {
            // ISWALK = true;
            animator.SetBool("ISWALK", true);
        }
        else
        {
            //   ISWALK = false;
            animator.SetBool("ISWALK", false);
        }

        velocity.y += Physics2D.gravity.y * Time.deltaTime;
        transform.Translate(velocity * Time.deltaTime);
        grounded = false;

        // Retrieve all colliders we have intersected after velocity has been applied.
        Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, boxCollider.size, 0);

        foreach (Collider2D hit in hits)
        {
            // Ignore our own collider.
            if (hit == boxCollider)
                continue;

            ColliderDistance2D colliderDistance = hit.Distance(boxCollider);

            // Ensure that we are still overlapping this collider.
            // The overlap may no longer exist due to another intersected collider
            // pushing us out of this one.
            if (colliderDistance.isOverlapped)
            {
                transform.Translate(colliderDistance.pointA - colliderDistance.pointB);

                // If we intersect an object beneath us, set grounded to true. 
                if (Vector2.Angle(colliderDistance.normal, Vector2.up) < 90 && velocity.y < 0)
                {
                    grounded = true;
                }
            }
        }
    }
}
