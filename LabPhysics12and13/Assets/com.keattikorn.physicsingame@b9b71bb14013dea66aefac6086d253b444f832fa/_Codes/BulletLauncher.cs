﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLauncher : MonoBehaviour {
    float _currentAngle = 90;
    float _length = 1.5f;
    const float ANGLE_STEP = 3;

    Vector3 _initialPosition;
    public float _bulletMass = 0.1f; 
    public float _impulseForce;
	// Use this for initialization
	void Start () {
        _initialPosition = this.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        float sine = Mathf.Sin(_currentAngle*Mathf.Deg2Rad);
        float cosine = Mathf.Cos(_currentAngle * Mathf.Deg2Rad);
        Vector3 unitCircleDirection = new Vector3(_length * cosine, 0, _length * sine);
        this.transform.position = _initialPosition+ unitCircleDirection;

        if (Input.GetKey(KeyCode.LeftArrow)) {
            _currentAngle += ANGLE_STEP;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            _currentAngle -= ANGLE_STEP;
        }
        if (Input.GetKeyDown(KeyCode.Z)) {
            GameObject sphereBullet = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphereBullet.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
            sphereBullet.transform.position = (_initialPosition + unitCircleDirection*1.5f);
            Rigidbody rb = sphereBullet.AddComponent<Rigidbody>();
            rb.mass = _bulletMass;
            rb.AddForce(unitCircleDirection.normalized * _impulseForce, ForceMode.Impulse);
            rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            Destroy(sphereBullet,5);
        }
        Debug.DrawLine(_initialPosition, this.transform.position,Color.red);
    }
}

