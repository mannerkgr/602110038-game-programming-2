﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace KSResearch.Utility
{
public class Utilities : MonoBehaviour {

    public static void CreateVectorGameObjectAt(
        Vector3 startPosition, 
        Vector3 direction, 
        float magnitude, float showTime)
    {
        GameObject go = new GameObject();
        go.name = "GizmosVector";
        DrawVectorGizmos dvg = go.AddComponent<DrawVectorGizmos>();
        dvg.StartPosition = startPosition;
        dvg.Direction = direction;
        dvg.LineColor = Color.red;
        dvg.Length = magnitude;

        Destroy(go, showTime);        
    }

    /*
    public static Queue CopyQueue(Queue q) {
        Queue copy = new Queue();
        Queue copySecond = new Queue();

        while(q.Count > 0)        
        {
            object obj = q.Dequeue();
            copy.Enqueue(Utilities.Clone<object>(obj));
            copySecond.Enqueue(Utilities.Clone<object>(obj));
        }

        q = copySecond;
        return copy;
    }

    public static T Clone<T>(T source)
    {
        if (!typeof(T).IsSerializable)
        {
            throw new ArgumentException("The type must be serializable.", "source");
        }

        // Don't serialize a null object, simply return the default for that object
        if (object.ReferenceEquals(source, null))
        {
            return default(T);
        }

        IFormatter formatter = new BinaryFormatter();
        Stream stream = new MemoryStream();
        using (stream)
        {
            formatter.Serialize(stream, source);
            stream.Seek(0, SeekOrigin.Begin);
            return (T)formatter.Deserialize(stream);
        }
    }
    */
}
}