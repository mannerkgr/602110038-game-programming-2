﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionMenumanage : MonoBehaviour
{
    [SerializeField] Dropdown _dropdownDifficulty;
    [SerializeField] Toggle _toggleMusic;
    [SerializeField] Toggle _toggleSFX;
    [SerializeField] Button _backButton;
    // Start is called before the first frame update
    void Start()
    {
        _dropdownDifficulty.value = GameApplicationManagement.Instance.DifficultyLevel;
        _toggleMusic.isOn = GameApplicationManagement.Instance.MusicEnabled;
        _toggleSFX.isOn = GameApplicationManagement.Instance.SFXEnabled;
        _dropdownDifficulty.onValueChanged.AddListener(delegate {
            DropdownDifficultyChanged(
                _dropdownDifficulty);
        });
        _toggleMusic.onValueChanged.AddListener(delegate { OnToggleMusic(_toggleMusic); });
        _toggleSFX.onValueChanged.AddListener(delegate { OnToggleSFX(_toggleSFX); });
        _backButton.onClick.AddListener(delegate { BackButtonClick(_backButton); });

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void BackButtonClick(Button button)
    {
        SceneManager.UnloadSceneAsync("SceneOptions");
        GameApplicationManagement.Instance.IsOptionMenuActive = false;
    }

    public void DropdownDifficultyChanged(Dropdown dropdown)
    {
        GameApplicationManagement.Instance.DifficultyLevel = dropdown.value;
    }

    public void OnToggleMusic(Toggle toggle)
    {
        GameApplicationManagement.Instance.MusicEnabled = _toggleMusic.isOn;
    }
    public void OnToggleSFX(Toggle toggle)
    {
        GameApplicationManagement.Instance.SFXEnabled = _toggleSFX.isOn;
    }
}