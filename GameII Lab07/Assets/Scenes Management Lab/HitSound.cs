﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class HitSound : MonoBehaviour
{
     AudioSource _hit;
   
    // Start is called before the first frame update
    void Start()
    {

        _hit = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter (Collision other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            _hit.Play();
        }
    }
}
