﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuControlScript : MonoBehaviour, IPointerEnterHandler
{
    // Start is called before the first frame update
    [SerializeField] Button _startButton;
    [SerializeField] Button _optionsButton;
    [SerializeField] Button _helpButton;
    [SerializeField] Button _exitButton;
    [SerializeField] Button _soundTestingButton;

    AudioSource _audioSource;
    [SerializeField] AudioClip _holdOverClip;

    void Start () {
        _audioSource = this.GetComponent<AudioSource>();
        _audioSource.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];

        SetupButtonsDelegate();

        if (!SoundManager.Instance.BGMSource.isPlaying)
            SoundManager.Instance.BGMSource.Play();
    }
 
    // Update is called once per frame
    void Update () {

    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_audioSource.isPlaying)
            _audioSource.Stop();

        _audioSource.PlayOneShot(_holdOverClip);

    }
    void SetupButtonsDelegate()
    {

        _startButton.onClick.AddListener(
                delegate { StartButtonClick(_startButton); });

        _optionsButton.onClick.AddListener(
            delegate { OptionsButtonClick(_optionsButton); });

        _helpButton.onClick.AddListener(
            delegate { HelpButtonClick(_helpButton); }); 

        _exitButton.onClick.AddListener(
            delegate { ExitButtonClick(_exitButton); });

        _soundTestingButton.onClick.AddListener(
           delegate { SoundTestingButtonClick(_soundTestingButton); });

    }
    public void StartButtonClick (Button button) {
        SceneManager.LoadScene ("SceneGameplay");
    }

    public void OptionsButtonClick (Button button) {
        if (!GameApplicationManager.Instance.IsOptionMenuActive) {
            SceneManager.LoadScene ("SceneOptions", LoadSceneMode.Additive);
            GameApplicationManager.Instance.IsOptionMenuActive = true;
        }
    }

    public void HelpButtonClick(Button button)
    {
        SceneManager.LoadScene("SceneHelp");
    }

    public void SoundTestingButtonClick (Button button)
    {
        if (SoundManager.Instance.BGMSource.isPlaying)
            SoundManager.Instance.BGMSource.Stop();
        SceneManager.LoadScene("SceneSoundTesting");
    }
    public void ExitButtonClick (Button button) {
        Application.Quit ();
    }

}