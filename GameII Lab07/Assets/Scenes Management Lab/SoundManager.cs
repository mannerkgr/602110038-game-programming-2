﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public const float MUTE_VOLUME = -80;

    static public SoundManager Instance
    {
        get
        {
            if (_singleton_instance == null)
            {
                _singleton_instance = GameObject.FindObjectOfType<SoundManager>();
                GameObject container = new GameObject("SoundManager");
                _singleton_instance = container.AddComponent<SoundManager>();
            }
            return _singleton_instance;
        }
    }
    static protected SoundManager _singleton_instance = null;

    [SerializeField]
    AudioMixer _mixer;
    public AudioMixer Mixer
    {
        get { return _mixer; }
    }

    public float MusicVolume
    {
        get
        {
            return _musicVolume;
        }

        set {
            _musicVolume = value;
            SoundManager.Instance.Mixer.SetFloat("MusicVolume", _musicVolume);
        }
    }
    float _musicVolume;
    public float MusicVolumeDefault
    {
        get
        {
            return _musicVolumeDefault;
        }
    }
    float _musicVolumeDefault;

    public float MasterSFXVolume
    {
        get
        {
            return _masterSFXVolume;
        }
    
    set{ _masterSFXVolume = value;
        SoundManager.Instance.Mixer.SetFloat("MasterSFXVolume", _masterSFXVolume);
        }}
float _masterSFXVolume;
public float MasterSFXVolumeDefault
{
 get
    {
        return _masterSFXVolumeDefault;
         }
 }
 float _masterSFXVolumeDefault;

 AudioSource _audioSourceBGM;
public AudioSource BGMSource
{
 get
    {
         return _audioSourceBGM;
         }
 }

 void Awake()
{
     if (_singleton_instance == null)
    {
         _singleton_instance = this;
         DontDestroyOnLoad(this.gameObject);
        
     //Important, execute these statements only 1st time singleton instance creation
     float musicVolume;
         if (_mixer.GetFloat("MusicVolume", out musicVolume))
             _singleton_instance._musicVolumeDefault = musicVolume;
         float masterSFXVolume;
         if (_mixer.GetFloat("MasterSFXVolume", out masterSFXVolume))
             _singleton_instance._masterSFXVolumeDefault = masterSFXVolume;
         }
    else
    {
         if (this != _singleton_instance)
        {
             Destroy(this.gameObject);
             }
         }
     }
void Start()
{
    _audioSourceBGM = this.GetComponent<AudioSource>();

}
void Update()
{

}
}



// Start is called before the first frame update


