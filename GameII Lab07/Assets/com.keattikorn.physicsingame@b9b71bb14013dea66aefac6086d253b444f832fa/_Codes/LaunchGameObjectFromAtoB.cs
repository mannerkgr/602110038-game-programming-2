﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using KSResearch.Utility;

public class LaunchGameObjectFromAtoB : MonoBehaviour
{
    Vector3 _pointA;
    Vector3 _pointB;

    [Header("GameObject to be launched")]
    [SerializeField]
    GameObject _gameobject = null;

    [SerializeField]
    float _impulseForceMagnitude = 5;

    // Start is called before the first frame update
    void Start()
    {
        _pointA = this.transform.position; 
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null)
                {
                    _pointB = hit.point;
                    Vector3 shootDir = (_pointB - _pointA).normalized;
                    
                    Launch(shootDir,_impulseForceMagnitude);
                    Utilities.CreateVectorGameObjectAt(_pointB, shootDir, _impulseForceMagnitude,3);
                }
            }
        }
    }

    void Launch(Vector3 dir, float mag){
        GameObject go = null;
        Rigidbody rb = null;

        if(_gameobject == null){
            go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            rb = go.AddComponent<Rigidbody>();
            go.AddComponent<GlueTrapActor>();
            go.AddComponent<Light>();
        }else {
            go = Instantiate(_gameobject,_pointA,Quaternion.identity);
            rb = go.GetComponent<Rigidbody>();
            if(rb == null){
                rb = go.AddComponent<Rigidbody>();
            }
        }
        
        go.transform.position = _pointA;

        rb.AddForce(dir*mag,ForceMode.Impulse);
    }
}
