﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlObjectMovementOnXZPlaneUsingArrowKeys : MonoBehaviour {
    [SerializeField]
    private float _movementStep = 0.1f;
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKey (KeyCode.LeftArrow)) {
            this.transform.Translate (-_movementStep, 0, 0);
        } else if (Input.GetKey (KeyCode.RightArrow)) {
            this.transform.Translate (_movementStep, 0, 0);
        } else if (Input.GetKey (KeyCode.UpArrow)) {
            this.transform.Translate (0, 0, _movementStep);
        } else if (Input.GetKey (KeyCode.DownArrow)) {
            this.transform.Translate (0, 0, -_movementStep);
        }

    }
}