﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class PlayerCollisionCheck : MonoBehaviour
{
    public TextMeshPro _textMesh = null;
    // Start is called before the first frame update
    void Start()
    {
    _textMesh = this.GetComponentInChildren<TextMeshPro>();    
    }

    // Update is called once per frame
    void Update()
    {
    
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Item")
        {
            Destroy(collision.gameObject);
        } else if (collision.gameObject.tag == "Obstacle") {
            Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
            rb.AddForce(0,10,0,ForceMode.Impulse);
            Destroy (collision.gameObject,2f);
        }


        TextMesh _textMesh = this.gameObject.GetComponentInChildren<TextMesh>();
        ItemTypeComponent itc = collision.gameObject.GetComponent<ItemTypeComponent>();
        if(itc != null){
            switch(itc.Type){
                case ItemType.BIGCOIN:
                 _textMesh.text = "BIGCOIN";
                break;
                case ItemType.BOX_ITEM:
                
                    _textMesh.text = "Box Item";
                
                break;
                case ItemType.COIN:
                 _textMesh.text = "COIN";
                break;
                case ItemType.CYLINDER_OBSTACLE:
                 _textMesh.text = "CYLINDER_OBSTACLE";
                break;
                case ItemType.POWERUP:
                 _textMesh.text = "POWERUP";
                break;
                case ItemType.POWERDOWN:
                 _textMesh.text = "POWERDOWN";
                break;
                case ItemType.SPHERE_OBSTACLE:
                 _textMesh.text = "SPHERE_OBSTACLE";
                break;
            }
        }
    }
}
