﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypeComponent : MonoBehaviour
{
    [SerializeField]
    // Start is called before the first frame update
    protected ItemType _itemtype;

    public ItemType Type{
        get{
return _itemtype;
        }
        set{
_itemtype = value;
        }
    }
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
