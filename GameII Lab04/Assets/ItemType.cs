﻿public enum ItemType{
    COIN,
    BIGCOIN,
    POWERUP,
    POWERDOWN,
    BOX_ITEM,
    SPHERE_OBSTACLE,
    CYLINDER_OBSTACLE
}