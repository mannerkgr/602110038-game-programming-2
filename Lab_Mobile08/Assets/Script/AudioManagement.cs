﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagement : MonoBehaviour
{
    public AudioClip m_wingAudio;
    public AudioClip m_deadAudio;
    public AudioClip m_pointAudio;
    AudioSource m_source;
    public static AudioManagement instance;
    // Use this for initialization
    void Awake()
    {
        if (instance == null)
            instance = this;
        m_source = GetComponent<AudioSource>();
    }
    public void PlayWing()
    {
        m_source.PlayOneShot(m_wingAudio, m_source.volume);
    }
    public void PlayLost()
    {
        m_source.PlayOneShot(m_deadAudio, m_source.volume);
    }
    public void PlayPlusPoint()
    {
        m_source.PlayOneShot(m_pointAudio, m_source.volume);
    }
}