﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies_Move : MonoBehaviour
{

    [SerializeField]
    public int HP;
    [SerializeField]
    private Vector3 _velocity;
    public Vector3 Velocity
    {
        get
        {
            return _velocity;
        }
        set
        {
            this._velocity = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.tag = "Enemies";
    }

    // Update is called once per frame
    void Update()
    {

        this.transform.position += _velocity * Time.deltaTime;
        
        if (HP == 0)
        {
            Destroy(this.gameObject);
        }
        

    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Bullet")
        {
            HP = HP - 1;
        }

        if (collision.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }

    }



}
