﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Shooter : MonoBehaviour
{
    Vector3 _initialPosition;
    [SerializeField]
    public float _bulletMass = 0.1f;
    [SerializeField]
    public float _Force;
    [SerializeField]
    public float bullet_lifetime;
    [SerializeField]
    public int reload_delay;
    float _time = 0;

    public GameObject BulletTest;

    // Start is called before the first frame update
    void Start()
    {

        _initialPosition = this.transform.position;

    }

    // Update is called once per frame
    void Update()
    {

        _time = _time + Time.deltaTime;

        if (_time > reload_delay)
        {

            GameObject sphereBullet = Instantiate(BulletTest, _initialPosition + new Vector3(0.5f, 0.0f, 0.0f), Quaternion.identity) as GameObject;
            sphereBullet.gameObject.tag = "Bullet";

            //sphereBullet.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            //sphereBullet.transform.position = _initialPosition + new Vector3(0.0f, 0.0f, 0.0f);
            Rigidbody rb = sphereBullet.AddComponent<Rigidbody>();
            rb.AddForce(_Force, 0.0f, 0.0f);
            rb.collisionDetectionMode = CollisionDetectionMode.Discrete;

            Destroy(sphereBullet, 10);


            _time = 1;
        }
    }

}
