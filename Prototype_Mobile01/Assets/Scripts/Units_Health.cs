﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Units_Health : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.tag = "Player";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Foe")
        {
            Debug.Log("Die");
            Destroy(this.gameObject);
        }

    }
}
