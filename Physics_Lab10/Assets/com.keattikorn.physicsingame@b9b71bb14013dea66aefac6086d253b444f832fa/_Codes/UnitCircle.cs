﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KSResearch
{
    public class UnitCircle : MonoBehaviour
    {

        [SerializeField]
        [Tooltip("The current Theta angle in degree of the unit circle")]
        private float _thetaAngleDegree = 0.0f;
        public float Theta
        {
            get
            {
                return _thetaAngleDegree;
            }
        }

        public float SetThetaInDegree
        {
            set
            {
                _thetaAngleDegree = value;
            }
        }
        public float SetThetaInRadian
        {
            set
            {
                _thetaAngleDegree = Mathf.Rad2Deg * value;
            }
        }

        public Vector2 UnitCircleXYComponents
        {
            get
            {
                float x, y;
                x = Mathf.Cos(_thetaAngleDegree);
                y = Mathf.Sin(_thetaAngleDegree);
                return new Vector2(x, y);
            }
        }

        public float XComponent
        {
            get
            {
                return Mathf.Cos(_thetaAngleDegree);
            }
        }

        public float YComponent
        {
            get
            {
                return Mathf.Sin(_thetaAngleDegree);
            }
        }


        // Use this for initialization
        public virtual void Start()
        {

        }

        // Update is called once per frame
        public virtual void Update()
        {

        }

    }
}