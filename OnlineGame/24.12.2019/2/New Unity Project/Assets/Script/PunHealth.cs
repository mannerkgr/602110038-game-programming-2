﻿using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PunHealth : MonoBehaviourPunCallbacks, IPunObservable
{
    public int maxHealth = 100;
    public int currentHealth;
    public void OnGUI()
    {
        if (photonView.IsMine)
            GUI.Label(new Rect(0, 0, 300, 50), "Player Health : " + currentHealth);
    }
    public void TakeDamage(int amount, int OwnerNetID)
    {
        if (photonView != null)
            photonView.RPC("PunRPCTakedDamage", RpcTarget.All, amount, OwnerNetID);
        else print("photonView is NULL.");
    }
    [PunRPC]
    public void PunRPCTakedDamage(int amount, int OwnerNetID)
    {
        Debug.Log("Take Damage");
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            Debug.Log("BulletID : " + OwnerNetID.ToString() + " Killed" +
           photonView.ViewID);
            currentHealth = maxHealth;
        }
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(currentHealth);
        }
        else
        {
            currentHealth = (int)stream.ReceiveNext();
        }
    }
    public void Healing(int amout)
    {
        currentHealth += amout;
    }

    public int CurrentHealth
    {
        set
        {
            currentHealth = value;
            if (_healthBar != null)
                _healthBar.value = (float)currentHealth / (float)maxHealth;
        }
        get
        {
            return currentHealth;
        }
    }
    public Slider _healthBar;
    public void Initialize(int MaxHealth)
    {
        maxHealth = MaxHealth;
        this.CurrentHealth = maxHealth;
    }
}