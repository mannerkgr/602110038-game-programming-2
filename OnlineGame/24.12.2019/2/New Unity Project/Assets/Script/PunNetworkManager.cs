﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using System;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class PunNetworkManager : ConnectAndJoinRandom, IOnEventCallback
{
    private readonly byte RandomCallAirDropEvent = 10;
    bool IsGameStart = false;
    bool isFirstSetting = false;
    public GameObject HealingPrefab;
    public int numberOfHealing = 5;
    float m_count = 0;
    public float m_CountDownDropHeal = 10;

    public static PunNetworkManager singleton;
    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;
    private bool IsGameOver;

    private void StartGame()
    {
        Camera main = Camera.main;
        if (main.name == "Camera")
            main.gameObject.SetActive(false);
        SpawnPlayer();
    }

    private void Awake()
    {
        singleton = this;
        StartGame();
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        //Can't Run Why?
        StartGame();
    }

    public void SpawnPlayer()
    {
        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
    PhotonNetwork.Instantiate(GamePlayerPrefab.name,
 new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
        IsGameStart = true;
    }
    private void AirDropHealing()
    {
        if (isFirstSetting == false)
        {
            isFirstSetting = true;
            int half = numberOfHealing / 2;
            for (int i = 0; i < half; i++)
            {

                PhotonNetwork.InstantiateSceneObject(HealingPrefab.name
                 , AirDrop.RandomPosition(5f)
                 , AirDrop.RandomRotation()
                 , 0);
            }
            m_count = m_CountDownDropHeal;
        }
        else
        {
            if (GameObject.FindGameObjectsWithTag("Healing").Length < numberOfHealing)
            {
                m_count -= Time.deltaTime;
                if (m_count <= 0)
                {
                    m_count = m_CountDownDropHeal;
                    PhotonNetwork.Instantiate(HealingPrefab.name
                    , AirDrop.RandomPosition(10f)
                    , AirDrop.RandomRotation()
                    , 0);
                }
            }
        }
    }
  

    public override void OnEnable()
    {
        base.OnEnable();
        PunGameTimer.OnCountdownTimerHasExpired += OnCountdownTimerIsExpired;
        // Raise Event
        PhotonNetwork.AddCallbackTarget(this);
        // LoadBalancingClient.EventReceived
        // The second way to receive custom events is to register a method
        //PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    public override void OnDisable()
    {
        base.OnDisable();
        PunGameTimer.OnCountdownTimerHasExpired -= OnCountdownTimerIsExpired;
        // Raise Event
        PhotonNetwork.RemoveCallbackTarget(this);
        // LoadBalancingClient.EventReceived
        // The second way to receive custom events is to register a method
        //PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    private void OnCountdownTimerIsExpired()
    {
        if (AutoConnect == false)
            StartGame();
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        object GameOver;
        if
       (propertiesThatChanged.TryGetValue(PunGameSetting.GAMEOVER,
       out GameOver))
        {
            Debug.Log("Game Over.");
            IsGameOver = (bool)GameOver;
        }
    }

    public void OnEvent(EventData photonEvent)
    {
        Debug.Log(photonEvent.ToStringFull());
        byte eventCode = photonEvent.Code;
        if (eventCode == RandomCallAirDropEvent)
        {

            Debug.Log("Call Resise Event is : " + eventCode.ToString());
            object[] data = (object[])photonEvent.CustomData;
            Vector3 position = (Vector3)data[0];
            Quaternion rotation = (Quaternion)data[1];
            int color = (int)data[2];
            Debug.Log("Position : " + position);
            Debug.Log("Rotation : " + rotation);
            Debug.Log("Color : " + color);
            // Instance Local Object
            GameObject localHeal = Instantiate(HealingPrefab, position, rotation);
            Color currentColor = PunGameSetting.GetColor(color);
            localHeal.GetComponent<MeshRenderer>().material.color = currentColor;
        }
    }
    private void CallRaiseEvent()
    {
        // Array contains the target position and the IDs of the selected units
        object[] content = new object[] { AirDrop.RandomPosition(5f),AirDrop.RandomRotation(), UnityEngine.Random.Range(0, 7) };

        // You would have to set the Receivers to All in order to receive this event on the local client as well
 RaiseEventOptions raiseEventOptions = new RaiseEventOptions
 {
     Receivers =ReceiverGroup.All
 };
        SendOptions sendOptions = new SendOptions { Reliability = true };
        PhotonNetwork.RaiseEvent(RandomCallAirDropEvent, content, raiseEventOptions,
       sendOptions);
        Debug.Log("Call Raise Event.");
    }
    private void Update()
    {
        if (PhotonNetwork.IsMasterClient != true)
            return;
        if (IsGameStart == true)
        {
            if (isFirstSetting == false)
                PunGameTimer.StartTime();
            AirDropHealing();
            // Raise Event create healing with host only.
            if (Input.GetKeyDown(KeyCode.H))
            {
                CallRaiseEvent();
            }
        }
    }
   
}
