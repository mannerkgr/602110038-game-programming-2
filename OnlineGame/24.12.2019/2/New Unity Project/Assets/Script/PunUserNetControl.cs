﻿
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback, IOnEventCallback
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;
    public Text _textNickname;
    public MeshRenderer currentMeshRenderer;
    public enum PunUserEvent
    {
        ChangeColorEv = 0
    }

    private void ChangeColorProperties()
    {
        Hashtable props = new Hashtable
 {
 { PunGameSetting.PLAYER_COLOR, Random.Range(0,7) }
 };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    }
    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);
        
        if (changedProps.ContainsKey(PunGameSetting.PLAYER_COLOR) && target.ActorNumber == photonView.ControllerActorNr)
        {
            
            object colors;
            if (changedProps.TryGetValue(PunGameSetting.PLAYER_COLOR, out colors))
            {
                Debug.Log("Good Job" + PunGameSetting.GetColor((int)colors).ToString());
                //   GetComponent<MeshRenderer>().material.color = PunGameSetting.GetColor((int)colors);
                currentMeshRenderer.material.color = PunGameSetting.GetColor((int)colors);
            }

            return;
        }
    }

    private void ChangeColorRaiseEvent()
    {
        float r, g, b;
        r = Random.Range(0f, 1f);
        g = Random.Range(0f, 1f);
        b = Random.Range(0f, 1f);
        // Array contains the viewID and the color of the selected units
        object[] content = new object[] { this.photonView.ViewID, r, g, b };
        // You would have to set the Receivers to All in order to receive this event
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions()
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.DoNotCache
        };
        SendOptions sendOptions = new SendOptions()
        {
            Reliability = true
        };
        PhotonNetwork.RaiseEvent((byte)PunUserEvent.ChangeColorEv,
        content, raiseEventOptions, sendOptions);
    }


    public void OnEvent(EventData photonEvent)
    {
        Debug.Log(photonEvent.ToStringFull());
        byte eventCode = photonEvent.Code;
        if (eventCode == (byte)PunUserEvent.ChangeColorEv)
        {
            Debug.Log("Call PunUserEvent Resise Event");
            object[] data = (object[])photonEvent.CustomData;
            //Correct View ID or myself
            if (this.photonView.ViewID == (int)data[0])
            {
                Debug.Log("Data " + (float)data[1] + " : " + (float)data[2] + " : " + (float)data[3]);
                Color _newColor = new Color((float)data[1], (float)data[2], (float)data[3]);
                MeshRenderer _mesh = GetComponent<MeshRenderer>();
                _mesh.material.color = _newColor;
            }
        }
    }
    
   
    
   
  
    /*public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        //PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        //PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }*/
 
    private void Awake()
    {
        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
        }
        else
        {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponent<FirstPersonController>().enabled = false;
        }
    }
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());
        if (photonView.IsMine)
        {
            info.Sender.TagObject = this.gameObject;
            LocalPlayerInstance = gameObject;
            //UI Control
            GetComponentInChildren<Canvas>().renderMode =
           RenderMode.ScreenSpaceCamera;
            GetComponentInChildren<UIDirectionControl>().enabled = false;
            SettingPlayerColor(info.photonView.Owner.CustomProperties);
            SettingPlayerHealth(info.photonView.Owner.CustomProperties);
        }
        else
        {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponentInChildren<AudioListener>().enabled = false;
            GetComponent<FirstPersonController>().enabled = false;
        }
        if (_textNickname != null)
            _textNickname.text = info.Sender.NickName;
    }

    private void SettingPlayerHealth(Hashtable changedProps)
    {
        object health;
        if (changedProps.TryGetValue(PunGameSetting.PLAYER_LIVES, out health))
        {
            GetComponent<PunHealth>().Initialize((int)health);
        }
    }
    private void SettingPlayerColor(Hashtable changedProps)
    {
        object colors;
        if (changedProps.TryGetValue(PunGameSetting.PLAYER_COLOR, out colors))
        {
            GetComponent<MeshRenderer>().material.color =
           PunGameSetting.GetColor((int)colors);
        }
    }
    void Update()
    {
        if (!photonView.IsMine)
            return;

        if (Input.GetKeyDown(KeyCode.C))
        {
            ChangeColorProperties();
        }
    }


}
