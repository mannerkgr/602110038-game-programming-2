﻿using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine;
using UnityEngine.UI;
using System;
using Photon.Realtime;

public class PunGameTimer : MonoBehaviourPunCallbacks
{
    public delegate void CountdownTimerHasExpired();
    /// <summary>
    /// Called when the timer has expired.
    /// </summary>
    public static event CountdownTimerHasExpired OnCountdownTimerHasExpired;
    private bool isTimerRunning;
    private float startTime;
    [Header("Reference to a Text component for visualizing the countdown")]
    public Text Text;
    [Header("Countdown time in seconds")]
    public float Countdown = 120f;


    public void Start()
    {
        if (Text == null)
        {
            Debug.LogError("Reference to 'Text' is not set.Please set a valid reference.", this);
            return;
        }
    }
    public void Update()
    {
        if (!isTimerRunning)
        {
            if (PhotonNetwork.IsMasterClient == true)
                return;
            Room Current = PhotonNetwork.CurrentRoom;
            if (Current != null)
            {
                object startTimeFromProps;
                if (Current.CustomProperties.
                TryGetValue(PunGameSetting.START_GAMETIME,
                out startTimeFromProps))
                {
                    Debug.Log("Start Game.");
                    isTimerRunning = true;
                    Debug.Log(startTimeFromProps);
                    startTime = (float)startTimeFromProps;
                }
            }
            return;
        }
        float timer = (float)PhotonNetwork.Time - startTime;
        float countdown = Countdown - timer;
        Text.text = "Time : " + CovertformatTime(countdown);
        if (countdown > 0.0f)
        {
            return;
        }
        isTimerRunning = false;
        Text.text = string.Empty;
        if (OnCountdownTimerHasExpired != null)
        {
            OnCountdownTimerHasExpired();
        }
        string CovertformatTime(float seconds)
        {
            double hh = Math.Floor(seconds / 3600),
            mm = Math.Floor(seconds / 60) % 60,
            ss = Math.Floor(seconds) % 60;
            return hh.ToString("00") + ":" + mm.ToString("00") +
           ":" + ss.ToString("00");
        }
    }
        public static void StartTime()
    {
        Hashtable props = new Hashtable {
 {PunGameSetting.START_GAMETIME, (float) PhotonNetwork.Time}
 };
        PhotonNetwork.CurrentRoom.SetCustomProperties(props);
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        object startTimeFromProps;
        if
       (propertiesThatChanged.TryGetValue(PunGameSetting.START_GAMETIME, out startTimeFromProps))
        {
            Debug.Log("Start Game.");
            isTimerRunning = true;
            startTime = (float)startTimeFromProps;
        }
    }

}
/// OnCountdownTimerHasExpired delegate.
/// </summary>



