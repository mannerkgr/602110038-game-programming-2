﻿using UnityEngine;
using Photon.Pun;
public class PunPlayerController : MonoBehaviourPun
{
    public GameObject bulletPrefab;
    public Camera m_playerCam;
    public float BulletForce = 20f;
    void Start()
    {
        m_playerCam = this.GetComponentInChildren<Camera>();
    }
    void Update()
    {
        if (!photonView.IsMine)
            return;
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            CmdFire(m_playerCam.transform.rotation);
        }
    }
    void CmdFire(Quaternion rotation)
    {
        //Keep Data when Instantiate.
        object[] data = { photonView.ViewID };
        // Spawn the bullet on the Clients and Create the Bullet from the Bullet Prefab
        Rigidbody bullet = PhotonNetwork.Instantiate(this.bulletPrefab.name , this.transform.position + (this.transform.forward * 1.5f), rotation , 0 , data).GetComponent<Rigidbody>();
        // Add velocity to the bullet
        bullet.velocity = bullet.transform.forward * BulletForce;
        // Destroy the bullet after 10 seconds
        PhotonView.Destroy(bullet.gameObject, 10.0f);
    }
}