﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class Healing : MonoBehaviourPun
{
    public int healingPoint = 1;
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Test other : " + other.gameObject.name);
        if (other.gameObject.CompareTag("Player"))
        {
            PunHealth otherHeal = other.gameObject.GetComponent<PunHealth>();
            otherHeal.Healing(healingPoint);
            // RaiseEvent with Local GameObject.
            if (photonView.ViewID <= 1000)
            {
                photonView.RPC("PunRPCHealing", RpcTarget.MasterClient);
            }
            else
            {
                // Local control
                Destroy(this.gameObject);
            }

        }
    }
    [PunRPC]
    private void PunRPCHealing()
    {
        Destroy(this.gameObject);
    }
    private void OnDestroy()
    {
        // RaiseEvent with Local GameObject.
        if (!photonView.IsMine || photonView.ViewID <= 1000)
            return;
        PhotonNetwork.Destroy(this.gameObject);
    }
}
