﻿using UnityEngine;

public class UIDirectionControl : MonoBehaviour {
    
    public Transform LocalDirection;
    
	// Use this for initialization
	void Start () {
        Camera currentCam = null;
        GameObject[] list = GameObject.FindGameObjectsWithTag("Player");
        for(int i = 0; i < list.Length; i++) {
            if(list[i].GetComponentInChildren<Camera>().enabled) {
                currentCam = list[i].GetComponentInChildren<Camera>();
            }
        }
        LocalDirection = currentCam.transform;

        RectTransform[] textlist = GetComponentsInChildren<RectTransform>();
        for(int i = 0; i < textlist.Length; i++) {
            textlist[i].Rotate(Vector3.up, 180);
        }
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.LookAt(LocalDirection);
    }
}
