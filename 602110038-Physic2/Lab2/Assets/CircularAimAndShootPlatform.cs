﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularAimAndShootPlatform : MonoBehaviour
{
    const float ANGLE_STEP = 3;
    float _currentAngle = 90;
    float _length = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float sine = Mathf.Sin(_currentAngle*Mathf.Deg2Rad);
        float cosine =Mathf.Cos(_currentAngle*Mathf.Deg2Rad);
        Vector3 unitCircleDirection = new Vector3 (_length*cosine,_length*sine,0);
        this.transform.position = unitCircleDirection;
    if (Input.GetKey(KeyCode.LeftArrow))
 {
 _currentAngle += ANGLE_STEP;
 }
 if (Input.GetKey(KeyCode.RightArrow))
 {
      _currentAngle -= ANGLE_STEP;
 }
if (Input.GetKeyDown(KeyCode.UpArrow))
 {
GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
 sphere.transform.position = this.transform.position * 2;
 Rigidbody rb = sphere.AddComponent <Rigidbody >();
 rb.useGravity = false;
 rb.AddForce(unitCircleDirection.normalized * 3,ForceMode.Impulse);
 Destroy(sphere ,2);
 }
 }
 }