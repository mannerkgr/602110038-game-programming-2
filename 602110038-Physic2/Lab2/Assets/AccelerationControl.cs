﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VelocityControl))]
public class AccelerationControl : MonoBehaviour
{
    [SerializeField]
    private Vector3 _acceleration;
    public Vector3 Acceleration{
        get
        {
            return _acceleration;
        }
        set{
            _acceleration = value;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        VelocityControl vc = this.GetComponent <VelocityControl >();
Vector3 v = vc.Velocity;
v += _acceleration * Time.deltaTime;
vc.Velocity = v;


    }
}
