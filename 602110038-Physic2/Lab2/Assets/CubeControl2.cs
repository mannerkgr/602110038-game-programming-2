﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeControl2 : MonoBehaviour
{
    Vector3 _sphereMovementStep = new Vector3(-0.03f,-0.05f,0);
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += _sphereMovementStep;
        if (this.transform.position.x >= 3.0f || this.transform.position.x <= -4.0f) 
        {
        _sphereMovementStep.x *= -1;
        }
        if (this.transform.position.y >= 3.0f || this.transform.position.y <= -4.0f){
        _sphereMovementStep.y *= -1;
    }
    if (this.transform.position.z >= 3.0f || this.transform.position.z <= -4.0f){
        _sphereMovementStep.z *= -1;
    }if (Input.GetMouseButtonDown(0)) {
        GameObject cube = GameObject.Find("Cube");
        Vector3 cubePosition = cube.transform.position;
        Vector3 vecToCubeNorm = cubePosition - this.transform.position;

        vecToCubeNorm.Normalize();


        this._sphereMovementStep = vecToCubeNorm/10.0f;}
}
    }
    
