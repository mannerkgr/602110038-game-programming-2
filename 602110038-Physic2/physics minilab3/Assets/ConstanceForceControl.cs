﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ConstanceForceControl : MonoBehaviour
{
    [Header("UI SETTINGS")]
    [SerializeField]
   public TextMeshPro _textVelocity = null;

    [Header ("Force Settings")]
    [SerializeField]
    [Tooltip("Consance force applying to an object.")]
    private Vector3 _force;
    public Vector3 Force
    {
        get
        {
            return _force;
        }
        set
        {
            _force = value;
        }
    }

    [Header("Time Settings")]
    [SerializeField]
    private bool _isLimitTime = false;
    [SerializeField]
    private float _timeToApplyForce = 1.0f;
    private float _timeCounter = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_isLimitTime){
            if (_timeCounter <= _timeToApplyForce){
                _timeCounter += Time.deltaTime;
                ApplyForce();
            }
        }
        else{
            ApplyForce();
        }
        DisplayVelocityText();
    }
    private void DisplayVelocityText(){
        if (_textVelocity != null){
            Rigidbody rb = this.GetComponent <Rigidbody>();
            _textVelocity.text = rb.velocity.ToString();
        }
    }
    private void ApplyForce(){
        Rigidbody rb = this.GetComponent<Rigidbody>();
        rb.AddForce(_force);
    }
}

