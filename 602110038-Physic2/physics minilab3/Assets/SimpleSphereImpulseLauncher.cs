﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleSphereImpulseLauncher : MonoBehaviour
{
    [SerializeField]
    float _mass;

    [SerializeField]
    float _forceMagnitude;

    [SerializeField]
    float _launcherInterval;

    // Start is called before the first frame update
    void Start()
    {
    Invoke("LaunchBall", _launcherInterval);    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void LaunchBall(){
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        go.transform.position = this.transform.position;

        Rigidbody rb = go.AddComponent<Rigidbody>();
        rb.mass = _mass;

        rb.AddForce(this.transform.forward* _forceMagnitude, ForceMode.Impulse);

        Invoke("LaunchBall", _launcherInterval);
    }
}
