﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorControl : MonoBehaviour
{
    public GameObject[] listMeteors;
    public Transform MainCircle;
    public Transform positionLeancher;
    public void CreateMeteor()
    {
        int m_angle = Random.Range(0, 360);
        MainCircle.transform.Rotate(new Vector3(0, 0, 1), m_angle);
        int MeteorSize = Random.Range(0, listMeteors.Length);
        GameObject obj = Instantiate(listMeteors[MeteorSize]);
        obj.GetComponent<MoveInertia>().Initl(positionLeancher);
    }
}

