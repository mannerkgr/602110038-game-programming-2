﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class ShipControl : MonoBehaviour {

    //Value Attribute
    public int PowerSpeed = 3;
    public float RotationSpeed = 0.5f;
	public GameObject m_Laser;
	Rigidbody2D m_Rigid;

	void Start() {
		m_Rigid = GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () {
		float v = CrossPlatformInputManager.GetAxis("Vertical");
		Vector3 MoveForward = new Vector3 (0,v);
		MoveForward = this.transform.TransformDirection (MoveForward);
		m_Rigid.AddForce (MoveForward * PowerSpeed);

		float h = CrossPlatformInputManager.GetAxis("Horizontal");
		m_Rigid.AddTorque (h * RotationSpeed * -1);

		//Fire Laser
		if (CrossPlatformInputManager.GetButtonDown ("Fire1")) {
			GameObject la = Instantiate (m_Laser);
			la.transform.position = this.transform.position;
			la.transform.rotation = this.transform.rotation;
			la.GetComponent<Rigidbody2D> ().AddForce (la.transform.up * 5, ForceMode2D.Impulse);
		}
	}

	public void FlipPosition(OppositeSide side) {
		Vector3 newPos = Vector3.zero;
		switch (side) {
			case OppositeSide.Left: 
			case OppositeSide.Right:
                    newPos.x = this.transform.position.x * -1;
                    newPos.y = this.transform.position.y;
				break;
			case OppositeSide.Top:
			case OppositeSide.Bottom:
					newPos.x = this.transform.position.x;
                    newPos.y = this.transform.position.y * -1;
            break;
		}
		this.transform.position = newPos;
	}
}
