﻿using UnityEngine;
using UnityEngine.UI;

public class NumberControl : MonoBehaviour {

    public Sprite[] listNumbers;
    Image m_sprite;
    public int UnitOfNumber = 1;

    private void Awake() {
        m_sprite = GetComponent<Image>();
    }

    public void UpdateNumber(int number) {

        number = (number / UnitOfNumber) % 10;
		/*
        if(m_sprite == null)
            m_sprite = GetComponent<Image>();
		*/
        m_sprite.sprite = listNumbers[number];
    }
}
