﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchSides : MonoBehaviour
{
    public OppositeSide m_side = OppositeSide.None;
 void OnTriggerEnter2D(Collider2D other) {
 if (other.tag.Contains ("Player")) {
 other.GetComponent<ShipControl> ().FlipPosition (m_side);
 } else if (other.tag.Contains ("Laser")) {
 Destroy (other.gameObject);
 }
 }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
