﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GameManagement : MonoBehaviour
{
    [SerializeField] private int m_score = 0;
    public ScoreControl _scorecontrol;
    [SerializeField] private int m_life = 3;
    public NumberControl _lifeNumber;
    [SerializeField] private bool isGameOver = false;
    public Text GameOver;
    public float countDown;
    public float delay = 5;
    MeteorControl m_MeteorController;
    public static GameManagement instance = null;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    void Start()
    {
        m_MeteorController = GetComponent<MeteorControl>();
        countDown = delay;
        _lifeNumber.UpdateNumber(m_life);
        PlusScore(0);
    }
    void Update()
    {
        if (isGameOver == false)
        {
            countDown -= Time.deltaTime;
            if (countDown <= 0)
            {
                countDown = delay;
                m_MeteorController.CreateMeteor();
            }
        }
        if (m_life <= 0 && !isGameOver)
        {
            m_life = 0;
            isGameOver = true;
            print("Game Over");
            GameOver.enabled = true;
        }
    }
    public int GetScore()
    {
        return m_score;
    }
    public void PlusScore(float factor)
    {
        m_score += (int)(10 * factor);
        _scorecontrol.UpdateScore(m_score);
    }
    public int GetLife()
    {
        return m_life;
    }
    public void SetLife(int life)
    {
        m_life = life;
        _lifeNumber.UpdateNumber(m_life);
    }
    public bool IsGameOver()
    {
        return isGameOver;
    }
}
