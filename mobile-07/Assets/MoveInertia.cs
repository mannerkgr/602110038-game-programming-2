﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveInertia : MonoBehaviour
{
    Rigidbody2D m_Rigid;
    Vector2 m_Force;
    [SerializeField]
    [Range(0.1f, 2f)] float Force = 2f;
    [Range(1f, 3f)] public float FactorScore = 1;
    public void Initl(Transform trans)
    {
        this.transform.position = trans.position;
        m_Force = this.transform.position * Random.Range(1, Force) * -1;
        m_Rigid = GetComponent<Rigidbody2D>();
        m_Rigid.AddForce(m_Force, ForceMode2D.Impulse);
        Destroy(this.gameObject, 5);
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.tag.Contains("Laser"))
        {
            print("Boom by Laser.");
            GameManagement.instance.PlusScore(FactorScore);
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
        else if (other.collider.tag.Contains("Player"))
        {
            print("Boom by Player.");
            GameManagement.instance.SetLife (GameManagement.instance.GetLife () - 1);
            Destroy(this.gameObject);
        }
    }
}