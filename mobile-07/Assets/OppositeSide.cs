﻿using UnityEngine;
using System.Collections;
public enum OppositeSide
{
    None,
    Left,
    Right,
    Top,
    Bottom
}
