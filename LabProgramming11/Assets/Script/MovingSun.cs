﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingSun : MonoBehaviour
{
    [SerializeField]
    float _sunStep = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(1, 0, 0), _sunStep * Time.deltaTime);
    }
}
