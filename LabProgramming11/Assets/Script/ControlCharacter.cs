﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCharacter : MonoBehaviour
{
    public float _movementStep;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            this.transform.Translate(-_movementStep, 0, 0);
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            this.transform.Translate(_movementStep, 0, 0);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            this.transform.Translate(0, 0,_movementStep);
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            this.transform.Translate(0, 0,-_movementStep);
        }

    }
}
